<?php

/**
 * index.php
 *
 * Redirects to the index.html file.
 *
 * @copyright 1999-2024 The SquirrelMail Project Team
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version $Id: index.php 15002 2024-01-02 22:26:51Z pdontthink $
 * @package squirrelmail
 */

header('Location: index.html');

