@echo off
color 0
title FSI Server Benchmark
cls
set java=java
rem JDK required for -server option
rem set java=C:\Program Files\Java\jdk1.6.0_23\bin\java
set jars=..\..\lib
cd /D %~dp0

set timeFactor=3
set tests=all
rem set tests=encoder,effects,image,swapimage
set subtests=all
rem set subtests=overload
set resultFile=result.txt

set javaOptions=-Djava.ext.dirs=%jars% -Djava.awt.headless=true -server
set vmOptions=-Xss256k -Xms1g -Xmx1g -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -XX:+UseParNewGC

%java% %javaOptions% %vmOptions% -jar %jars%\fsi.jar start runTimeFactor=%timeFactor% tests=%tests% subtests=%subtests% verbose resultFile=%resultFile%
