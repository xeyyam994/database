#!/bin/sh

java=java
#java=/usr/bin/java
jars=../../lib

runTime=180
tests=all
#tests=encoder,effects,image,swapimage
resultFile=result.txt

javaOptions="-Djava.ext.dirs=$jars -Djava.awt.headless=true -server"
vmOptions="-Xss256k -Xms1g -Xmx1g -XX:MaxPermSize=256m -XX:+UseConcMarkSweepGC -XX:+UseParNewGC"

if [ -n "$2" ]
then
  resultFile=$2
  format=$1
elif [ -n "$1" ]
then
  resultFile=$1
  format="txt"
else
  echo "Usage: $0 [txt|xml] result.txt"
  exit 0
fi

$java $javaOptions $vmOptions -jar $jars/fsi.jar start runTime=$runTime tests=$tests format=$format verbose resultFile=$resultFile
