-----------------------------------------------------------------------
FSI Server Benchmark - Manual
-----------------------------------------------------------------------

Valid Parameters for fsi.jar:
    start - start benchmark
    runTime=sec per test - default: 180
    priority=min|norm|max - default: norm
    tests=all,cache,image,encoder,swap,storage,io - default: all
    subtests=all,hotrun,full - default: all
    maxThreads=n - default: all (using all available CPU cores)
    workDir=dir - default: $TEMPDIR
    verbose=true|false - default: false
    resultFile=filename.txt - output file for benchmark result
    format=txt|xml - output file format

Recommended Java Options (64 Bit):
-Djava.awt.headless=true -server -Xss256k -Xms1g -Xmx1g -XX:+UseConcMarkSweepGC -XX:+UseParNewGC

Example 1 - Standard test settings:
java -Djava.awt.headless=true -server -Xss256k -Xms1g -Xmx1g -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -jar fsi.jar start

Example 2 - Testing IO and saving result to a file:
java -Djava.awt.headless=true -server -Xss256k -Xms1g -Xmx1g -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -jar fsi.jar start tests=io,swap,storage resultFile=result.txt

Preparation Notes:
Before benchmarking please disable all unnecessary running processes like file indexing, anti-virus apps, screensaver, raytracer, etc.
A complete benchmark with default values requires around 20 minutes.

-----------------------------------------------------------------------

Tests:
    cache: Test linear heap memory throughput (SYS)
    image: Test non-linear direct memory throughput (SYS)
    encoder: Test JVM JPEG encoding (SYS)
    swap: Test mapped memory (I/O) throughput (IO)
    io: Test file-system performance and disk read/write I/O throughput (IO)
    storage: Test disk read I/O with a simulated storage (IO)

Calibration:
    The test is calibrated to this system:

    Benchmark Version 3.0.144
    2x Intel XEON L5410 (Harpertown) @ 2.33GHz (4 Cores per CPU / no HT) and total 8 Cores
    4x 4GB FBDIMM 667
    Intel S5000PSL Board
    Adaptec 2420SA Raid-5 with 4x 750GB S-ATA Samsung (3.0Gb/s)
    Debian Linux 7.6 (Wheezy)
    Kernel 3.14-0.bpo.2-amd64 (deadline)
    Oracle Corporation Java HotSpot(TM) 64-Bit Server VM build 1.8.0_20-b26
    Filesystem ext4 with options noatime,barrier=0,commit=10,nouser_xattr
    VM Options: -Djava.awt.headless=true -server -Xss256k -Xms1g -Xmx1g -XX:+UseConcMarkSweepGC -XX:+UseParNewGC
    Start Options: start verbose

    Index of each subtest: 1000
    ---------------------------
    Total index SYS:       1000
    Total index IO :       1000
    ===========================


Performance Notes:
-----------------------------------------------------------------------

Especially the IO test is much(!) slower on windows system with NTFS compared to Linux with Ext4.
In general the underlying file-system is an important factor for FSI Server (but also for many other web-services).
Ext3 is very often 25-40% slower than Ext4.


Interpretation:
-----------------------------------------------------------------------

Glossary:
    TEIT: What the test is testing internally.
    TESU: Which subsystems are tested primarily.
    HICO: How important is the test in terms of FSI Core.
    HIIM: How important is the test in terms of FSI Importer.
    IHCS: Influence of the CPU single core speed.
    IHCC: Influence of the number of CPU cores.
    IHIO: Influence of the disk storage system (S-ATA, SAS, SAN, RAID, Disk Caches, etc.)
    ISOS: Influence of the OS.
    ISVM: Influence of the Java VM Version (Version 6-7, IBM, Oracle/Sun, OpenJDK).
    ISGC: Influence of the Java VM garbage collector choice.
    IMPR: How to improve this test.

*** Cache ***
    TEIT: This test tests the main part if the FSI Server byte cache.
    TESU: Memory bandwidth (e.g. Dual/Triple/Quad Channel + Frequency + Northbridge, if exists)
    HICO: Medium - the complete internal caching system works with parts of this test. It has a low impact on delivery performance.
    HIIM: Low
    IHCS: High
    IHCC: Low
    IHIO: None
    ISOS: Medium
    ISVM: Low
    ISGC: None
    IMPR: Usually newer server hardware has better memory throughput.

*** Image ***
    TEIT: Builds different integer direct memory objects and fills them. This reflects the performance of internal image generation and copying.
    TESU: Memory bandwidth and arithmetic test (e.g. Dual/Triple/Quad Channel + Frequency + Northbridge)
    HICO: High
    HIIM: Medium
    IHCS: High
    IHCC: High
    IHIO: None
    ISOS: Medium
    ISVM: None
    ISGC: Low
    IMPR: Increase CPU cores and use newer server hardware for better memory throughput.

*** Encoder ***
    TEIT: Tests encoding of differently sized images to JPEG. The test encodes a SSI pattern in 7 sizes in a typical access spreading. 
    TESU: Direct+Heap memory performance, CPU core performance
    HICO: High
    HIIM: Medium
    IHCS: High
    IHCC: High
    IHIO: None
    ISOS: Low
    ISVM: Low
    ISGC: Low
    IMPR: Use better CPU(s) with more cores and higher frequency.

*** Swap ***
    TEIT: Tests the performance of memory mapped file access.
    TESU: VM NIO memory mapping performance, disk I/O and filesystem.
    HICO: None
    HIIM: Low (but High if you have not enough memory to load e.g. very large images into RAM) 
    IHCS: High
    IHCC: High
    IHIO: Medium
    ISOS: Very High
    ISVM: High - Java 7 is much faster than Java 6
    ISGC: Medium - With -XX:+DoEscapeAnalysis in Java 6 the performance is ~50% faster.
    IMPR: Use better I/O subsystem, e.g. map work dir to SSD or use Hardware Raid or SAS or use -XX:+DoEscapeAnalysis to increase the performance

*** Storage ***
    TEIT: Tests the filesystem performance considering FSI Servers internal storage format version V1002
    TESU: Disk I/O, CPU cores, Filesystem
    HICO: High
    HIIM: High
    IHCS: Medium
    IHCC: Low
    IHIO: High
    ISOS: High
    ISVM: Low
    ISGC: Low
    IMPR: Use better I/O subsystem (Hardware Raid, SSD), better CPU, switch to a modern filesystem (e.g. ext4, btrfs), don't use virtualized storage

*** IO ***
    TEIT: Tests the filesystem performance considering FSI Servers internal storage format version V1002
    TESU: Disk I/O, CPU cores, Filesystem
    HICO: High
    HIIM: High
    IHCS: Medium
    IHCC: Low
    IHIO: High
    ISOS: High
    ISVM: Low
    ISGC: Low
    IMPR: Use better I/O subsystem (Hardware Raid, SSD), better CPU, switch to a modern filesystem (e.g. ext4, btrfs), don't use virtualized storage

    