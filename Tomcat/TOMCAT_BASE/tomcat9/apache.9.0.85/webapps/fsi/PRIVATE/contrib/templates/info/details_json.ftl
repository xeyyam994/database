<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'JSON',
        'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"general" : {
	"src":"${info.src}",
	<#if info.width??>"width":"${info.width}",</#if>
	<#if info.height??>"height":"${info.height}",</#if>
	<#if info.size??>"size":"${info.size}",</#if>
	<#if info.alpha??>"alpha":"${info.alpha}",</#if>
	<#if info.icc??>"icc":"${info.icc}",</#if>
	<#if info.importstatus??>"importstatus":"${info.importstatus}",</#if>
        <#if info.importtimestamp??>"importtimestamp":"${info.importtimestamp}",</#if>
	"lastmodified":"${info.lastmodified}"
},
"iptc" : {<#if info.iptclist??>
	<#list info.iptclist as iptc><#if morethanoneiptc??>,</#if>
	"${iptc.key}":"${iptc.name}"
<#assign morethanoneiptc=1></#list></#if>
},
"exif" : {<#if info.exiflist??>
	<#list info.exiflist as exif><#if morethanoneexif??>,</#if>
	"${exif.key}":"${exif.name}"
<#assign morethanoneexif=1></#list></#if>
},
"xmp" : {<#if info.xmp??>
	<#list info.xmp as xmp><#if morethanonexmp??>,</#if>
	"${xmp.key}":"${xmp.name}"
<#assign morethanonexmp=1></#list></#if>
}
}
<#if query?? && query.callback??>)</#if>
