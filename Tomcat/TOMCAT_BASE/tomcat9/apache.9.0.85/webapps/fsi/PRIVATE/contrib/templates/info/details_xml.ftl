<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi>
<Image>
	<src>${info.src}</src>
	<width>${info.width}</width>
	<height>${info.height}</height>
	<#if info.size??><size>${info.size}</size></#if>
	<#if info.alpha??><alpha>${info.alpha}</alpha></#if>
	<#if info.icc??><icc>${info.icc}</icc></#if>
	<#if info.importstatus??><importstatus>${info.importstatus}</importstatus></#if>
	<lastmodified>${info.lastmodified}</lastmodified>

	<#if info.iptc??><iptc>
	<#assign keys = info.iptc?keys>
		<#list keys as key><item name="${key}">${info.iptc[key]}</item>
	</#list></iptc>	
	</#if>

	<#if info.exif??><exif>
	<#assign keys = info.exif?keys>
		<#list keys as key><item name="${key}">${info.exif[key]}</item>
	</#list></exif>	
	</#if>

	<#if info.xmp??><xmp>
	<#assign keys = info.xmp?keys>
		<#list keys as key><item name="${key}">${info.xmp[key]}</item>
	</#list></xmp>	
	</#if>	

</Image> 
</fsi>
