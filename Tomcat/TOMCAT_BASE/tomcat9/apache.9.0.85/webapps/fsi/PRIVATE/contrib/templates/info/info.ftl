<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi>

	<Image>
		<Width value="<#if info.width??>${info.width}</#if>"/>
		<Height value="<#if info.height??>${info.height}</#if>"/>
	<#if info.iptc?? && info.iptc["FSI Tiles X"]?? >
		<TilesX value="${info.iptc["FSI Tiles X"]}"/>
		<TilesY value="${info.iptc["FSI Tiles Y"]}"/>
	</#if> 
	</Image>

	<#if info.iptc?? && info.iptc["FSI SceneSets"]??>
	<Options>
		<SceneSets>${info.iptc["FSI SceneSets"]}</SceneSets>
	</Options>
	</#if>

</fsi>
