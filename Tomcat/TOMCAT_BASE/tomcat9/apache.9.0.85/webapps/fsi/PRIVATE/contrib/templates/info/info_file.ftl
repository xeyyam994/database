<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi>

  <Image>
    <Path value="${info.src}"/>
	<#if info.width??><Width value="${info.width}"/></#if> 
	<#if info.height??><Height value="${info.height}"/></#if> 
	<#if info.iptc?? && info.iptc["FSI Tiles X"]?? >
	  <TilesX value="${info.iptc["FSI Tiles X"]}"/>
	  <TilesY value="${info.iptc["FSI Tiles Y"]}"/>
	</#if> 
  </Image>

  <Options>
    <#if info.iptc?? && info.iptc["FSI SceneSets"]??><SceneSets>${info.iptc["FSI SceneSets"]}</SceneSets></#if>
  	<#if info.size?? ><Info_File_Size>${info.size}</Info_File_Size></#if>
  	<#if info.fileformat?? ><Info_Img_Format>${info.fileformat}</Info_Img_Format></#if>
    <#if info.width?? && info.height??><Info_File_Dimension>${info.width} x ${info.height}</Info_File_Dimension></#if>
    <#if info.iptc?? && info.iptc["Objectname"]??><Iptc_Objectname>${info.iptc["Objectname"]}</Iptc_Objectname></#if>
    <#if info.iptc?? && info.iptc["Copyright"]??><Iptc_Copyright>${info.iptc["Copyright"]}</Iptc_Copyright></#if>
    <#if info.iptc?? && info.iptc["Caption"]??><Iptc_Caption>${info.iptc["Caption"]}</Iptc_Caption></#if>
  </Options>

</fsi>
