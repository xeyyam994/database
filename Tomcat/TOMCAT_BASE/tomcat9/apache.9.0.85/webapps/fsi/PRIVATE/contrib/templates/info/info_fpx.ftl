<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<info>
	<FPXWidth value="${info.width}"/>
	<FPXHeight value="${info.height}"/>
</info>
