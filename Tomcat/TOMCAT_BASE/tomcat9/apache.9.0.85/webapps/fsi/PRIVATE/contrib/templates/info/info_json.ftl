<#ftl encoding="UTF-8" attributes={
'define':{
    'Escape':'JSON',
    'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"src":"${info.src}",
"width":"${info.width}",
"height":"${info.height}",
<#if info.size??>"size":"${info.size}",</#if>
<#if info.alpha??>"alpha":"${info.alpha}",</#if>
<#if info.icc??>"icc":"${info.icc}",</#if>
<#if info.importstatus??>"importstatus":"${info.importstatus}",</#if>
"lastmodified":"${info.lastmodified}"}
<#if query?? && query.callback??>)</#if>
