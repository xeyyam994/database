<#ftl encoding="UTF-8" attributes={
'define':{
        'Content-Type':'application/xml; charset=UTF-8',
        'Escape':'XML'

}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi>
  
  <Image>
    <Path value="${info.src}"/>
	<#if info.width??><Width value="${info.width}"/></#if> 
	<#if info.height??><Height value="${info.height}"/></#if> 
  </Image>
  
  <#if info.iptc??>
    <Options>
	
	 <#if info.iptc["FSI SceneSets"]??>
		<SceneSets>${info.iptc["FSI SceneSets"]}</SceneSets>
	</#if>
	 <#if info.iptc["Caption"]??>
		<iptc_caption>${info.iptc["Caption"]}</iptc_caption>
	</#if>
    </Options>
  </#if>
  
</fsi>
