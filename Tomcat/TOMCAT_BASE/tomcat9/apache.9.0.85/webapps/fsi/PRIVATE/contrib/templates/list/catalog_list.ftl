<#ftl encoding="UTF-8" attributes={
'define':{
	'TypeFilter':'image',
	'Escape':'URIXML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <images FilePrefix="[fpxbase]" FileSuffix="&amp;type=info&amp;tpl=catalog_page">
	<#list ilist as entry><image file="<#if currentDir?? >${currentDir}</#if>${entry.src}" /></#list>
  </images>
</fsi>
