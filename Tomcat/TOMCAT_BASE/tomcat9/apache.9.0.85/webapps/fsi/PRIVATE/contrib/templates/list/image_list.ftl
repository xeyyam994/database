<#ftl encoding="UTF-8" attributes={
'define':{
	'TypeFilter':'image',
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <images>
		<#list ilist as entry>
		  <image>
		    <image>
		      <path value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
		      <width value="${entry.width}" />
		      <height value="${entry.height}" />
		      <#if entry.importstatus?? ><importStatus value="${entry.importstatus}" /></#if>
		    </image>
		  </image>
		</#list>
  </images>
</fsi>
