<#ftl encoding="UTF-8" attributes={
'define':{
	'TypeFilter':'image',
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <images3D>
		<#list ilist as entry>
		  <image path="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
		</#list>
  </images3D>
</fsi>
