<#ftl encoding="UTF-8" attributes={
'define':{
	'TypeFilter':'image',
	'Escape':'XML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <images3D>
		<#list ilist as entry>
		  <image path="<#if currentDir1?? >${currentDir1}</#if>${entry.src1},<#if currentDir2?? >${currentDir2}</#if>${entry.src2}" />
		</#list>
  </images3D>
</fsi>
