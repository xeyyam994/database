<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'URIXML',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <images FilePrefix="[fpxbase]" FileSuffix="&amp;type=info&amp;tpl=image_list3d">
	<#list ilist as entry>
	  <#if entry.type?matches("image") >
		<image label="${entry.src}" >
		  <fpx>
		    <src value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
		  </fpx>
		</image>
	  <#else>
	    <image label="${entry.src}" file="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
	  </#if>
	</#list>
  </images>
</fsi>
