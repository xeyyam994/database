<#ftl encoding="UTF-8" attributes={
'define':{
	'TypeFilter':'image',
	'Escape':'JSON',
        'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"images" : [<#list ilist as entry>{"src":"${entry.src}"<#if entry.width??>,"width":"${entry.width}"</#if><#if entry.height??>,"height":"${entry.height}"</#if>},</#list>{}]}
<#if query?? && query.callback??>)</#if>
