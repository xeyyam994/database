<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
        'Protected':'true',
        'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <images>
	<#list ilist as entry>
	  <#if entry.type?matches("image") >
		<image>
		  <image>
		    <path value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
			<#if entry.alpha??><alpha value="${entry.alpha}" /></#if>
		  </image>
		</image>
	  <#else>
		<image>
			<image>
				<type value="dir" />
				<path value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
			</image>
		</image>
	  </#if>
	</#list>
  </images>
</fsi>
