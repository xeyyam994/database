<#ftl encoding="UTF-8" attributes={
'define':{
        'Protected':'true',
	'TypeFilter':'directory',
	'Escape':'JSON',
        'Content-Type':'application/json; charset=UTF-8'
}
}>
<#-- directory information for interface.treeView control in JSON format-->
<#if query?? && query.callback??>${query.callback}(</#if>
[<#list ilist as entry><#if morethanone??>,</#if>{"title":"${entry.src}"<#if entry.hasSub?? && entry.hasSub>,"isLazy":true</#if>}<#assign morethanone=1></#list>]
<#if query?? && query.callback??>)</#if>
