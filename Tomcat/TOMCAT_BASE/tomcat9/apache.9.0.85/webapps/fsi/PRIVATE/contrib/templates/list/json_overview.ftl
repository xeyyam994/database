<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"images" : [<#list ilist as entry><#if morethanone??>,</#if>{<#if entry.exists ><#if (entry.imageCount > 0)>"src":"${entry.src}","width":"${entry.width}","height":"${entry.height}",</#if>"imageCount":"${entry.imageCount}"</#if>}<#assign morethanone=1></#list>,{}]}
<#if query?? && query.callback??>)</#if>
