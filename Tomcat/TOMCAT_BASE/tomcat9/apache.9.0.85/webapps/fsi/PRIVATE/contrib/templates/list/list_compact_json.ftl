<#ftl encoding="UTF-8" attributes={
'define':{
        'Protected':'true',
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#-- directory information for interface.thumbView control in JSON format-->
<#if query?? && query.callback??>${query.callback}(</#if>
{"summary":{<#if currentDir?? >"dir":"${currentDir}",</#if>"entryCount":${summary.entryCount},"imageCount":${summary.imageCount},"directoryCount":${summary.directoryCount},"completeCount":${summary.completeCount},"lastModified":${summary.lastModified}<#if summary.message?? >,"message":"${summary.message}"</#if>},
<#if restrictions?? >"restrictions":{"readOnly":${restrictions.readOnly},"writeEnabled":${restrictions.writeEnabled},"downloadOrigin":${restrictions.downloadOrigin}},</#if>
"entries" : [<#list ilist as entry>{<#if entry.id??>"id":"${entry.id}",</#if>"src":"${entry.src}",<#if entry.size??>"size":"${entry.size}",</#if><#if entry.lastmodified??>"lastmodified":"${entry.lastmodified}",</#if><#if entry.width??>"width":"${entry.width}",</#if><#if entry.height??>"height":"${entry.height}",</#if><#if entry.importstatus??>"importstatus":"${entry.importstatus}",</#if><#if entry.hasSub??>"sub":"${entry.dirs}","images":"${entry.images}",</#if>"type":"${entry.type}"},
</#list>{}]}
<#if query?? && query.callback??>)</#if>
