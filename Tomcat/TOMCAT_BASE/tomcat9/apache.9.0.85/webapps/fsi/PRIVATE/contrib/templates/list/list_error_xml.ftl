<#ftl encoding="UTF-8" attributes={
'define':{
        'Protected':'true',
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <directories><#list ilist as entry><#if entry.type?matches("directory") >
    <path><#if currentDir?? >${currentDir}</#if>${entry.src}</path></#if></#list>
  </directories>
  <images><#list ilist as entry><#if entry.importstatus?? && entry.importstatus == 3>
    <path><#if currentDir?? >${currentDir}</#if>${entry.src}</path></#if></#list>
  </images>
</fsi>
