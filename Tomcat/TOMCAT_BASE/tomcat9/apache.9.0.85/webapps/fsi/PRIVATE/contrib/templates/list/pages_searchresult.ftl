<#ftl encoding="UTF-8" attributes={
'define':{
	'TypeFilter':'image',
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<results>
  <#list ilist as entry><item searchID="<#if currentDir?? >${currentDir}</#if>${entry.src}" /></#list>
</results>
