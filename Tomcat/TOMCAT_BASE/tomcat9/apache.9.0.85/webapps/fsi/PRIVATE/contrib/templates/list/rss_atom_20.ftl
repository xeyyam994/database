<#ftl encoding="UTF-8" attributes={
'define':{
    'TypeFilter':'image',
    'Escape':'XML',
    'Content-Type':'application/rss+xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>

	<atom:icon>interface/skins/black/images/cooliris_logo.png</atom:icon>
	<title>Images</title>
	<author><name>FSI Viewer Server</name></author>

	<description>${currentDir}</description>

<#list ilist as entry>
    <#if entry.importstatus == 1 && entry.type?matches("image")>
	<item>
		<title>${entry.src}</title>
		<guid>${entry.src}</guid>
		<media:description>${entry.width}x${entry.height}</media:description>
		<#if query.thumbsize??>
			<media:thumbnail url="?type=image&amp;source=${currentDir}${entry.src}&amp;width=${query.thumbsize}&amp;height=${query.thumbsize}&amp;quality=85" />
		<#else>
			<media:thumbnail url="?type=image&amp;source=${currentDir}${entry.src}&amp;width=200&amp;height=200&amp;quality=85" />
		</#if>
		<#if query.imagesize??>
			<media:content url="?type=image&amp;source=${currentDir}${entry.src}&amp;width=${query.imagesize}&amp;height=${query.imagesize}&amp;quality=95" type="image/jpeg" />
		<#else>
			<media:content url="?type=image&amp;source=${currentDir}${entry.src}&amp;width=900&amp;height=900&amp;quality=95" type="image/jpeg" />
		</#if>
	</item>
	</#if>
</#list>
</channel>
</rss>
