<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}>
<images>
<#list ilist as entry><#if entry.exists><#if (entry.imageCount > 0)>
	<image>
		<src value="${entry.src}" />
		<width value="${entry.width}" />
		<height value="${entry.height}" />
		<count value="${entry.imageCount}" />
	</image>
	</#if></#if>
</#list>
</images>
