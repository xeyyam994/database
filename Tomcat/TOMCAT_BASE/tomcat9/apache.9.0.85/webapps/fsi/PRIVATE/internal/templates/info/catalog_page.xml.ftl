<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage'
},
'define':{
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi:FSI xmlns:fsi="http://www.fsi-viewer.com/schema">

	<image>
		<Path value="${info.src}" />
		<#if info.width??><Width value="${info.width}"/></#if>
		<#if info.height??><Height value="${info.height}"/></#if>
<#if info.iptc?? && info.iptc["FSI Tiles X"]?? && info.iptc["FSI Tiles Y"]??>
		<TilesX value="${info.iptc["FSI Tiles X"]}"/>
		<TilesY value="${info.iptc["FSI Tiles Y"]}"/>
</#if> 
	</image>

	<#if info.iptc?? && info.iptc["FSI SceneSets"]?? >
	<Options>
		<SceneSets value="${info.iptc["FSI SceneSets"]}" />
	</Options>
	</#if>

	<#if info.iptc?? && info.iptc["FSI Extra"]?? >
	<Pages>
	${entityFixer(info.iptc["FSI Extra"])}
	</Pages>
	</#if>

</fsi:FSI>
