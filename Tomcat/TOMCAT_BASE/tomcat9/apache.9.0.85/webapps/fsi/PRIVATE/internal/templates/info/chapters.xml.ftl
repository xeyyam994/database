<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage'
},
'define':{
	'Content-Type':'application/xml; charset=UTF-8'
}
}><#if info.fsi?? && info.fsi['chapters']?? >${info.fsi['chapters']}<#else><?xml version="1.0" encoding="UTF-8" ?>
<indexdata />
</#if>
