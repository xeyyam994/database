<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage',
	'EXIF':'storage',
	'XMP':'storage'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{
<#if info.src??>"src":"${info.src}",</#if>
<#if info.width??>"width":"${info.width}",</#if>
<#if info.height??>"height":"${info.height}",</#if>
<#if info.size??>"size":"${info.size}",</#if>
<#if info.alpha??>"alpha":"${info.alpha}",</#if>
<#if info.icc??>"icc":"${info.icc}",</#if>
<#if info.levels??>"levels": [<#list info.levels as level><#if morethanonelevel??>,</#if>{"width":"${level.width}","height":"${level.height}","tileWidth":"${level.tilewidth}","tileHeight":"${level.tileheight}"}<#assign morethanonelevel=1></#list>],</#if>
<#if info.importstatus??>"importstatus":"${info.importstatus}",</#if>
<#if info.lastmodified??>"lastmodified":"${info.lastmodified}",</#if>
"iptc" : {<#if info.iptclist??>
	<#list info.iptclist as iptc><#if morethanoneiptc??>,</#if>
	"${iptc.key}":"${iptc.name}"
<#assign morethanoneiptc=1></#list></#if>
},
"exif" : {<#if info.exiflist??>
	<#list info.exiflist as exif><#if morethanoneexif??>,</#if>
	"${exif.key}":"${exif.name}"
<#assign morethanoneexif=1></#list></#if>
},
"xmp" : {<#if info.xmp??>
	<#list info.xmp as xmp><#if morethanonexmp??>,</#if>
	"${xmp.key}":"${xmp.name}"
<#assign morethanonexmp=1></#list></#if>
},
"fsi" : {<#if info.fsi??>
	<#list info.fsilist as fsi><#if morethanonefsi??>,</#if>
	"${fsi.key}":"${fsi.name}"
<#assign morethanonefsi=1></#list></#if>
}
}
<#if query?? && query.callback??>)</#if>
