<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage',
	'EXIF':'storage',
	'XMP':'storage'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi:FSI xmlns:fsi="http://www.fsi-viewer.com/schema">
<Image>
	<src>${info.src}</src>
	<#if info.width??><width>${info.width}</width></#if>
	<#if info.height??><height>${info.height}</height></#if>
	<#if info.size??><size>${info.size}</size></#if>
	<#if info.alpha??><alpha>${info.alpha}</alpha></#if>
	<#if info.icc??><icc>${info.icc}</icc></#if>
	<#if info.importstatus??><importstatus>${info.importstatus}</importstatus></#if>
	<lastmodified>${info.lastmodified}</lastmodified>

	<#if info.iptc??><iptc>
	<#assign keys = info.iptc?keys>
		<#list keys as key><item name="${key}">${info.iptc[key]}</item>
	</#list></iptc>	
	</#if>

	<#if info.exif??><exif>
	<#assign keys = info.exif?keys>
		<#list keys as key><item name="${key}">${info.exif[key]}</item>
	</#list></exif>	
	</#if>

	<#if info.xmp??><xmp>
	<#assign keys = info.xmp?keys>
		<#list keys as key><item name="${key}">${info.xmp[key]}</item>
	</#list></xmp>	
	</#if>	
	
	<#if info.fsi??><fsi>
	<#assign keys = info.fsi?keys>
		<#list keys as key><item name="${key}">${info.fsi[key]}</item>
	</#list></fsi>	
	</#if>

</Image> 
</fsi:FSI>
