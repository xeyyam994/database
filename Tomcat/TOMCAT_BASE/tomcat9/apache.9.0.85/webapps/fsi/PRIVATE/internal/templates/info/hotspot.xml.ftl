<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi:FSI xmlns:fsi="http://www.fsi-viewer.com/schema">

	<#if info.iptc?? && info.iptc["Caption"]?? >
	<HotSpots> 
	${info.iptc["Caption"]}
	</HotSpots>
	<#else>
	<HotSpots />
	</#if>
	<Image>
		<Path value="${info.src}"/>
		<#if info.width??><Width value="${info.width}"/></#if>
		<#if info.height??><Height value="${info.height}"/></#if>
	<#if info.iptc?? && info.iptc["FSI Tiles X"]?? && info.iptc["FSI Tiles Y"]??>
		<TilesX value="${info.iptc["FSI Tiles X"]}"/>
		<TilesY value="${info.iptc["FSI Tiles Y"]}"/>
	</#if> 
	</Image>

	<Plugins>
		<plugin src="hotspots" visible="true" DefaultMove="true" DefaultSkew="true" DefaultFill="true" />
	</Plugins>

	<#if info.iptc?? && info.iptc["FSI SceneSets"]??>
	<Options>
		<SceneSets>${info.iptc["FSI SceneSets"]}</SceneSets>
	</Options>
	</#if>

</fsi:FSI>
