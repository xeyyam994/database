<#ftl encoding="UTF-8" attributes={
'define':{
	'Content-Type':'application/xml; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi:FSI xmlns:fsi="http://www.fsi-viewer.com/schema">
	<#if info.fsi?? && info.fsi["HotSpots"]??>${info.fsi["HotSpots"]}</#if>
</fsi:FSI>
