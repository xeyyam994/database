<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{
<#if info.src??>"src":"${info.src}",</#if>
<#if info.width??>"width":"${info.width}",</#if>
<#if info.height??>"height":"${info.height}",</#if>
<#if info.size??>"size":"${info.size}",</#if>
<#if info.alpha??>"alpha":"${info.alpha}",</#if>
<#if info.icc??>"icc":"${info.icc}",</#if>
<#if info.levels??>"levels": [<#list info.levels as level><#if morethanonelevel??>,</#if>{"width":"${level.width}","height":"${level.height}","tileWidth":"${level.tilewidth}","tileHeight":"${level.tileheight}"}<#assign morethanonelevel=1></#list>],</#if>
<#if info.importstatus??>"importstatus":"${info.importstatus}",</#if>
<#if info.lastmodified??>"lastmodified":"${info.lastmodified}",</#if>
"_":1
}
<#if query?? && query.callback??>)</#if>
