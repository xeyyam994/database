<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi:FSI xmlns:fsi="http://www.fsi-viewer.com/schema">

	<Image>
		<Width value="<#if info.width??>${info.width}</#if>"/>
		<Height value="<#if info.height??>${info.height}</#if>"/>
	<#if info.iptc?? && info.iptc["FSI Tiles X"]?? && info.iptc["FSI Tiles Y"]?? >
		<TilesX value="${info.iptc["FSI Tiles X"]}"/>
		<TilesY value="${info.iptc["FSI Tiles Y"]}"/>
	</#if>
	<#if info.alpha??><Transparency value="${info.alpha}"/></#if>
	</Image>
	<#if info.iptc?? && info.iptc["FSI SceneSets"]??>
	<Options>
		<SceneSets>${info.iptc["FSI SceneSets"]}</SceneSets>
	</Options>
	</#if>

</fsi:FSI>
