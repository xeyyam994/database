<#ftl encoding="UTF-8" attributes={
'define':{
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
},
'access':{
	'AllowedGroups':'public'
},
'datasources':{
	'IPTC':'storage'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<info>
	<#if info.width??><FPXWidth value="${info.width}"/></#if>
	<#if info.height??><FPXHeight value="${info.height}"/></#if>
</info>
