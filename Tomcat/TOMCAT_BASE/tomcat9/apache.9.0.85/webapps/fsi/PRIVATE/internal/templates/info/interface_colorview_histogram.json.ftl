<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{
"fraction" : {<#if fraction??><#list fraction as val><#if morethanonefrac??>,</#if>
	"${val.key}":${val.name}<#assign morethanonefrac=1></#list></#if>
},
"min" : {<#if min??><#list min as val><#if morethanonemin??>,</#if>
	"${val.key}":${val.name}<#assign morethanonemin=1></#list></#if>
},
"max" : {<#if max??><#list max as val><#if morethanonemax??>,</#if>
	"${val.key}":${val.name}<#assign morethanonemax=1></#list></#if>
},
"redHistogram" : [<#if redHistogram??><#list redHistogram as val><#if morethanonered??>,</#if>${val}<#assign morethanonered=1></#list></#if>],
"greenHistogram" : [<#if greenHistogram??><#list greenHistogram as val><#if morethanonegreen??>,</#if>${val}<#assign morethanonegreen=1></#list></#if>],
"blueHistogram" : [<#if blueHistogram??><#list blueHistogram as val><#if morethanoneblue??>,</#if>${val}<#assign morethanoneblue=1></#list></#if>],
"hueHistogram" : [<#if hueHistogram??><#list hueHistogram as val><#if morethanonehue??>,</#if>${val}<#assign morethanonehue=1></#list></#if>],
"saturationHistogram" : [<#if saturationHistogram??><#list saturationHistogram as val><#if morethanonesat??>,</#if>${val}<#assign morethanonesat=1></#list></#if>],
"brightnessHistogram" : [<#if brightnessHistogram??><#list brightnessHistogram as val><#if morethanonebri??>,</#if>${val}<#assign morethanonebri=1></#list></#if>]
}
<#if query?? && query.callback??>)</#if>