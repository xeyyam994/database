<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{
"header" : {<#if header??><#list header as val><#if morethanoneheader??>,</#if>
	"${val.key}":"${val.name}"<#assign morethanoneheader=1></#list></#if>
},
"tags" : {<#if tags??><#list tags as val><#if morethanonetags??>,</#if>
	"${val.key}":"${val.name}"<#assign morethanonetags=1></#list></#if>
},
"whitePoint" : {<#if whitePoint??><#list whitePoint as val><#if morethanonewhitepoint??>,</#if>
	"${val.key}":${val.name}<#assign morethanonewhitepoint=1></#list></#if>
},
"blackPoint" : {<#if blackPoint??><#list blackPoint as val><#if morethanoneblackpoint??>,</#if>
	"${val.key}":${val.name}<#assign morethanoneblackpoint=1></#list></#if>
},
"redColorant" : {<#if redColorant??><#list redColorant as val><#if morethanredcolorant??>,</#if>
	"${val.key}":${val.name}<#assign morethanredcolorant=1></#list></#if>
},
"greenColorant" : {<#if greenColorant??><#list greenColorant as val><#if morethangreencolorant??>,</#if>
	"${val.key}":${val.name}<#assign morethangreencolorant=1></#list></#if>
},
"blueColorant" : {<#if blueColorant??><#list blueColorant as val><#if morethanbluecolorant??>,</#if>
	"${val.key}":${val.name}<#assign morethanbluecolorant=1></#list></#if>
}
}
<#if query?? && query.callback??>)</#if>