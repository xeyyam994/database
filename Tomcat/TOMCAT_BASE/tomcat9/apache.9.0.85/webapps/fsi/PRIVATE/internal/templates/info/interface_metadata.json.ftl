<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
},
'datasources':{
	'IPTC':'storage',
	'EXIF':'storage',
	'XMP':'storage',
	'SELECTIONS':'storage'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"general" : {
	<#if info.width??>"width":"${info.width}",</#if>
	<#if info.height??>"height":"${info.height}",</#if>
	<#if info.size??>"size":"${info.size}",</#if>
	<#if info.alpha??>"alpha":"${info.alpha}",</#if>
	<#if info.icc??>"icc":"${info.icc}",</#if>
	<#if info.importstatus??>"importstatus":"${info.importstatus}",</#if>
	<#if info.importtimestamp??>"importtimestamp":"${info.importtimestamp}",</#if>
	<#if info.connectortype??>"connectortype":"${info.connectortype}",</#if>
	<#if info.levels??>"levels":"${info.levels?size}",</#if>
	<#if info.connectorTotalContent??>"connectortotalcontent":"${info.connectorTotalContent}",</#if>
	<#if info.selections??>"selections" : [
		<#list info.selections as selection><#if morethanoneselection??>,</#if>
		"${selection}"
	<#assign morethanoneselection=1></#list>
	],</#if>
	<#if info.alphanames??>"alphachannels" : [
		<#list info.alphanames as alphaname><#if morethanonealpha??>,</#if>
		"${alphaname}"
	<#assign morethanonealpha=1></#list>
	],</#if>
	<#if info.lastmodified??>"lastmodified":"${info.lastmodified}",</#if>
	"src":"${info.src}"
},
"iptc" : {<#if info.iptclist??>
	<#list info.iptclist as iptc><#if morethanoneiptc??>,</#if>
	"${iptc.key}":"${iptc.name}"
<#assign morethanoneiptc=1></#list></#if>
},
"exif" : {<#if info.exiflist??>
	<#list info.exiflist as exif><#if morethanoneexif??>,</#if>
	"${exif.key}":"${exif.name}"
<#assign morethanoneexif=1></#list></#if>
},
"xmp" : {<#if info.xmp??>
	<#list info.xmp as xmp><#if morethanonexmp??>,</#if>
	"${xmp.key}":"${xmp.name}"
<#assign morethanonexmp=1></#list></#if>
},
"fsi" : {<#if info.fsi??>
	<#list info.fsilist as fsi><#if morethanonefsi??>,</#if>
	"${fsi.key}":"${fsi.name}"
<#assign morethanonefsi=1></#list></#if>
}
}
<#if query?? && query.callback??>)</#if>
