<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
    'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
},
'datasources':{
	'IPTC':'storage'
}
}>
<?xml version="1.0" encoding="UTF-8" ?> 
<fsi:FSI xmlns:fsi="http://www.fsi-viewer.com/schema">
  
  <Image>
    <Path value="${info.src}"/>
	<#if info.width??><Width value="${info.width}"/></#if>
	<#if info.height??><Height value="${info.height}"/></#if>
	<#if info.iptc?? && info.iptc["FSI Tiles X"]?? && info.iptc["FSI Tiles Y"]??>
	  <TilesX value="${info.iptc["FSI Tiles X"]}"/>
	  <TilesY value="${info.iptc["FSI Tiles Y"]}"/>
	</#if> 
  </Image>
  
  <Options>
    <#if info.iptc?? && info.iptc["FSI SceneSets"]??><SceneSets>${info.iptc["FSI SceneSets"]}</SceneSets></#if>
  	<#if info.iptc?? && info.iptc["Copyright"]??><Iptc_Copyright>${info.iptc["Copyright"]}</Iptc_Copyright></#if>
    <#if info.iptc?? && info.iptc["Caption"]??><Iptc_Caption>${info.iptc["Caption"]}</Iptc_Caption></#if>
  </Options>
  
  <plugins>
     <plugin src="textbox" height="80" bgAlpha="90" embedFonts="false" basecolor="DDDDDD" bgcolor="FFFFFF" textcolor="000000" TextSize="11" />
  </plugins>

</fsi:FSI>
