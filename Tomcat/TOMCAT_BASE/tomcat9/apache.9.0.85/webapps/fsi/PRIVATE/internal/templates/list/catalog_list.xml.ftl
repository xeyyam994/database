<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'URIXML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi:FSI xmlns:fsi="http://www.neptunelabs.com/schema/fsiviewer">
  <images FilePrefix="[fpxbase]" FileSuffix="&amp;type=info&amp;tpl=catalog_page.xml">
	<#list ilist as entry><image file="<#if currentDir?? >${currentDir}</#if>${entry.src}" /></#list>
  </images>
</fsi:FSI>