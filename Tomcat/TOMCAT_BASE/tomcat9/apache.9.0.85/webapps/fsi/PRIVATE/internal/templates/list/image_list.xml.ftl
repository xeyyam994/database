<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi:FSI xmlns:fsi="http://www.neptunelabs.com/schema/fsiviewer">
  <images>
		<#list ilist as entry>
		  <image>
		    <image>
		      <path value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
                      <#if entry.width??><width value="${entry.width}" /></#if>
		      <#if entry.height??><height value="${entry.height}" /></#if>
		      <#if entry.importstatus?? ><importStatus value="${entry.importstatus}" /></#if>
		    </image>
		  </image>
		</#list>
  </images>
</fsi:FSI>
