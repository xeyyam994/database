<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>{"imagesattributes":{<#if currentDir?? >"dir":"${currentDir}"</#if>},"images" : [<#list ilist as entry>{"src":"${entry.src}"<#if entry.width??>,"width":"${entry.width}"</#if><#if entry.height??>,"height":"${entry.height}"</#if>},</#list>{}]}
<#if query?? && query.callback??>)</#if>
