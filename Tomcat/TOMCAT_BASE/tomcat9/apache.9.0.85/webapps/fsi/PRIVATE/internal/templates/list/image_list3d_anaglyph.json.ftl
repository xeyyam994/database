<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>{"imagesattributes":{<#if currentDir?? >"dir":"${currentDir}"</#if>},"images" : [<#list ilist as entry>{"src":"<#if currentDir1?? >${currentDir1}</#if>${entry.src1},<#if currentDir2?? >${currentDir2}</#if>${entry.src2}"},</#list>{}]}
<#if query?? && query.callback??>)</#if>
