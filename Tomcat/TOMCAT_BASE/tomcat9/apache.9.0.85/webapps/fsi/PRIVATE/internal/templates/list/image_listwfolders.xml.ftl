<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi:FSI xmlns:fsi="http://www.neptunelabs.com/schema/fsiviewer">
  <images>
	<#list ilist as entry>
	  <#if entry.type?matches("file") >
		<image>
		  <image>
		    <path value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
			<#if entry.alpha??><alpha value="${entry.alpha}" /></#if>
		  </image>
		</image>
	  <#else>
		<image>
			<image>
				<type value="dir" />
				<path value="<#if currentDir?? >${currentDir}</#if>${entry.src}" />
			</image>
		</image>
	  </#if>
	</#list>
  </images>
</fsi:FSI>