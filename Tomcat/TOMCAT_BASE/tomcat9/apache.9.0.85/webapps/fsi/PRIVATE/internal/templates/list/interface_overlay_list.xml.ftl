<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'TypeFilter':'file',
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
  <files>
	<#list ilist as entry>
		<file>${entry.src}</file>
	</#list>
  </files>