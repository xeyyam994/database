<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#-- directory information for interface.thumbView control in JSON format-->
{"summary":{<#if currentDir?? >"dir":"${currentDir}",</#if>"entryCount":${summary.entryCount},"lastModified":${summary.lastModified}<#if summary.message?? >,"message":"${summary.message}"</#if>},
<#if restrictions?? >"restrictions":{"readOnly":${restrictions.readOnly},"writeEnabled":${restrictions.writeEnabled},"downloadOrigin":${restrictions.downloadOrigin}, "publicFileAccess":${restrictions.publicFileAccess}, "publicFileMetaAccess":${restrictions.publicFileMetaAccess}, "publicFileListAccess":${restrictions.publicFileListAccess}, "publicDirListAccess":${restrictions.publicDirListAccess}},</#if>
"entries" : [<#list ilist as entry>{<#if entry.id??>"id":"${entry.id}",</#if><#if entry.src??>"src":"${entry.src}",</#if><#if entry.fileName??>"fileName":"${entry.fileName}",</#if>"type":"file",<#if entry.size??>"size":"${entry.size}",</#if><#if entry.lastmodified??>"lastmodified":"${entry.lastmodified}",</#if><#if entry.createdBy??>"createdBy":"${entry.createdBy}",</#if><#if entry.creationDate??>"creationDate":"${entry.creationDate}",</#if><#if entry.scheduleDate??>"scheduleDate":"${entry.scheduleDate}",</#if><#if entry.startDate??>"startDate":"${entry.startDate}",</#if><#if entry.duration??>"duration":"${entry.duration}",</#if><#if entry.progress??>"progress":"${entry.progress}",</#if><#if entry.queuePos??>"queuePos":"${entry.queuePos}",</#if><#if entry.status??>"status":"${entry.status}"</#if>},
</#list>{}]}
