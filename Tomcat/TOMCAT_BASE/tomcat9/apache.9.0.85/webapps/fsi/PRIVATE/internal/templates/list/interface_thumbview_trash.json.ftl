<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#-- directory information for interface.thumbView control in JSON format-->
{"summary":{<#if currentDir?? >"dir":"${currentDir}",</#if>"entryCount":${summary.entryCount},"lastModified":${summary.lastModified}<#if summary.message?? >,"message":"${summary.message}"</#if>},
<#if restrictions?? >"restrictions":{"readOnly":${restrictions.readOnly},"writeEnabled":${restrictions.writeEnabled},"downloadOrigin":${restrictions.downloadOrigin}, "publicFileAccess":${restrictions.publicFileAccess}, "publicFileMetaAccess":${restrictions.publicFileMetaAccess}, "publicFileListAccess":${restrictions.publicFileListAccess}, "publicDirListAccess":${restrictions.publicDirListAccess}},</#if>
"entries" : [<#list ilist as entry>{<#if entry.id??>"id":"${entry.id}",</#if>"src":"${entry.src}",<#if entry.size??>"size":"${entry.size}",</#if><#if entry.lastmodified??>"lastmodified":"${entry.lastmodified}",</#if><#if entry.deletionDate??>"deleted":"${entry.deletionDate}",</#if><#if entry.sourcePath??>"sourcePath":"${entry.sourcePath}",</#if><#if entry.fileName??>"fileName":"${entry.fileName}"</#if>},
</#list>{}]}
