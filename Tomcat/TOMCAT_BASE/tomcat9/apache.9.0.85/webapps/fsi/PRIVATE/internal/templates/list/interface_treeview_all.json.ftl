<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#-- directory information for interface.treeView control in JSON format-->
<#if query?? && query.callback??>${query.callback}(</#if>
[<#list ilist as entry><#if morethanone??>,</#if>{"title":"${entry.src}"<#if entry.connectorType??>,"connectorType":"${entry.connectorType}"</#if><#if entry.type?? && entry.type == "directory">,"isLazy":true<#else>,"isFolder":false</#if>}<#assign morethanone=1></#list>]
<#if query?? && query.callback??>)</#if>
