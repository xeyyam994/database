<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'TypeFilter':'directory',
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#-- directory information for interface.treeView control in JSON format-->
<#if query?? && query.callback??>${query.callback}(</#if>
[<#list ilist as entry><#if morethanone??>,</#if>{"title":"${entry.src}"<#if entry.connectorType??>,"connectorType":"${entry.connectorType}"</#if><#if entry.connectorTotalContent??>,"connectorTotalContent":"${entry.connectorTotalContent}"</#if>
<#if entry.userpermissions?? >,
"permissions":{"read":
                   {"renderedImages":${entry.userpermissions.read.renderedImages?c},
                    "files":${entry.userpermissions.read.files?c},
                    "downloadSource":${entry.userpermissions.read.downloadSource?c},
                    "copy":${entry.userpermissions.read.copy?c},
                    "basicMetadata":${entry.userpermissions.read.basicMetadata?c},
                    "extendedMetadata":${entry.userpermissions.read.extendedMetadata?c},
                    "storageStatus":${entry.userpermissions.read.storageStatus?c},
                    "iccProfiles":${entry.userpermissions.read.iccProfiles?c},
                    "totalAssetsCount":${entry.userpermissions.read.totalAssetsCount?c}},
                   "list":
                   {"connector":${entry.userpermissions.list.connector?c},
                    "files":${entry.userpermissions.list.files?c},
                    "directories":${entry.userpermissions.list.directories?c},
                    "searchResults":${entry.userpermissions.list.searchResults?c}},
                   "write":
                   {"createDirectory":${entry.userpermissions.write.createDirectory?c},
                    "paste":${entry.userpermissions.write.paste?c},
                    "upload":${entry.userpermissions.write.upload?c},
                    "delete":${entry.userpermissions.write.delete?c},
                    "rename":${entry.userpermissions.write.rename?c},
                    "moveToTrash":${entry.userpermissions.write.moveToTrash?c},
                    "moveWithinConnector":${entry.userpermissions.write.moveWithinConnector?c},
                    "overwrite":${entry.userpermissions.write.overwrite?c},
                    "extendedMetadata":${entry.userpermissions.write.extendedMetadata?c}},
                   "tasks": 
                   {"reimportFiles":${entry.userpermissions.tasks.reimportFiles?c},
                    "batchRendering":${entry.userpermissions.tasks.batchRendering?c},
                    "createArchive":${entry.userpermissions.tasks.createArchive?c}}}
</#if>
<#if entry.publicpermissions?? >,
"publicPermissions":{"read":
                   {"renderedImages":${entry.publicpermissions.read.renderedImages?c},
                    "files":${entry.publicpermissions.read.files?c},
                    "downloadSource":${entry.publicpermissions.read.downloadSource?c},
                    "copy":${entry.publicpermissions.read.copy?c},
                    "basicMetadata":${entry.publicpermissions.read.basicMetadata?c},
                    "extendedMetadata":${entry.publicpermissions.read.extendedMetadata?c},
                    "storageStatus":${entry.publicpermissions.read.storageStatus?c},
                    "iccProfiles":${entry.publicpermissions.read.iccProfiles?c},
                    "totalAssetsCount":${entry.publicpermissions.read.totalAssetsCount?c}},
                   "list":
                   {"connector":${entry.publicpermissions.list.connector?c},
                    "files":${entry.publicpermissions.list.files?c},
                    "directories":${entry.publicpermissions.list.directories?c},
                    "searchResults":${entry.publicpermissions.list.searchResults?c}},
                   "write":
                   {"createDirectory":${entry.publicpermissions.write.createDirectory?c},
                    "paste":${entry.publicpermissions.write.paste?c},
                    "upload":${entry.publicpermissions.write.upload?c},
                    "delete":${entry.publicpermissions.write.delete?c},
                    "rename":${entry.publicpermissions.write.rename?c},
                    "moveToTrash":${entry.userpermissions.write.moveToTrash?c},
                    "moveWithinConnector":${entry.publicpermissions.write.moveWithinConnector?c},
                    "overwrite":${entry.publicpermissions.write.overwrite?c},
                    "extendedMetadata":${entry.publicpermissions.write.extendedMetadata?c}},
                   "tasks": 
                   {"reimportFiles":${entry.publicpermissions.tasks.reimportFiles?c},
                    "batchRendering":${entry.publicpermissions.tasks.batchRendering?c},
                    "createArchive":${entry.publicpermissions.tasks.createArchive?c}}}
</#if>
<#if entry.properties?? >,
"properties":{
<#assign keys = entry.properties?keys>
<#assign morethanoneprop=false>
<#list keys as key><#if morethanoneprop >,</#if>"${key}":["${entry.properties[key]?join("\", \"")}"]<#assign morethanoneprop=true></#list> 
}
</#if>
<#if entry.comment??>, "comment":"${entry.comment}"</#if><#if entry.hasSub?? && entry.hasSub>,"isLazy":true</#if>}<#assign morethanone=1></#list>]
<#if query?? && query.callback??>)</#if>
