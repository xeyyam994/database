<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'authenticated'
},
'define':{
	'Escape':'XML'
},
'headers':{
	'Content-Type':'application/xml; charset=UTF-8',
	'Cache-Control':'must-revalidate'
},
'headers-ssl':{
	'Content-Type':'application/xml; charset=UTF-8',
	'Cache-Control':'public, must-revalidate'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<fsi>
  <directories><#list ilist as entry><#if entry.type?matches("directory") >
    <path><#if currentDir?? >${currentDir}</#if>${entry.src}</path></#if></#list>
  </directories>
  <images><#list ilist as entry><#if entry.importstatus?? && entry.importstatus == 3>
    <path><#if currentDir?? >${currentDir}</#if>${entry.src}</path></#if></#list>
  </images>
</fsi>