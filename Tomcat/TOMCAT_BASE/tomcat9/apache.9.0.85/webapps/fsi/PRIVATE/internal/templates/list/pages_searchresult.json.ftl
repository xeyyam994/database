<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'JSON',
	'Content-Type':'application/json; charset=UTF-8'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"results" : [<#list ilist as entry>"${entry.src}",</#list>""]}
<#if query?? && query.callback??>)</#if>
