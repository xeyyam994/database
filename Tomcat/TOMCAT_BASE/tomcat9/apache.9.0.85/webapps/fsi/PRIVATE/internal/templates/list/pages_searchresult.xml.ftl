<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'XML',
	'Content-Type':'application/xml; charset=UTF-8'
}
}>
<?xml version="1.0" encoding="UTF-8" ?>
<results>
  <#list ilist as entry><item searchID="${entry.src}" /></#list>
</results>
