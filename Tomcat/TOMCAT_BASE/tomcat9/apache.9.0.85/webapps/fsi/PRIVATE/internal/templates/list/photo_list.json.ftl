<#ftl encoding="UTF-8" attributes={
'access':{
	'AllowedGroups':'public'
},
'define':{
	'TypeFilter':'file',
	'Escape':'JSON'
},
'headers':{
	'Content-Type':'application/json; charset=UTF-8',
	'Cache-Control':'must-revalidate',
	'Access-Control-Allow-Origin':'*'
},
'headers-ssl':{
	'Content-Type':'application/json; charset=UTF-8',
	'Cache-Control':'public, must-revalidate',
	'Access-Control-Allow-Origin':'*'
}
}>
<#if query?? && query.callback??>${query.callback}(</#if>
{"imagesattributes":{"FilePrefix":"[fpxbase]","FileSuffix":"&type=info&tpl=photo_page.xml"},"images":[<#list ilist as entry>{"src":"${entry.src}"<#if entry.width??>,"width":"${entry.width}"</#if><#if entry.height??>,"height":"${entry.height}"</#if>,"file":"[src]"},</#list>{}]}
<#if query?? && query.callback??>)</#if>
