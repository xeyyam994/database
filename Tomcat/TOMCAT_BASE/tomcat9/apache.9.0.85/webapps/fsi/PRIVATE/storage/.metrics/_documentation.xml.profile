<?xml version="1.0" encoding="UTF-8"?>
<SourceConnector>

  <Enabled>true</Enabled>

  <Type>static</Type>

  <Origin>
    <Accessor>filesystem</Accessor>
    <!--  relative to PRIVATE -->
    <Location>docs</Location>
  </Origin>

  <Access>
    <Group name="authenticated" permissionset="read_only"/>
  </Access>

  <Comment>
    Documentations
    Connector can be safely removed
  </Comment>

</SourceConnector>
