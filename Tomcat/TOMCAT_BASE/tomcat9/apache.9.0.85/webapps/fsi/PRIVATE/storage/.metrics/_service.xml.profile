<?xml version="1.0" encoding="UTF-8"?>
<SourceConnector>

  <Enabled>true</Enabled>

  <Type>storage</Type>

  <Origin>
    <Accessor>filesystem</Accessor>
    <!--  relative to PRIVATE -->
    <Location>internal/service_images</Location>
  </Origin>

  <Conversion>
    <Format>LOSSLESS</Format>
  </Conversion>

  <access>
    <group name="authenticated" permissionset="read_only"/>
    <group name="public" permissionset="public_images"/>
  </access>

</SourceConnector>
