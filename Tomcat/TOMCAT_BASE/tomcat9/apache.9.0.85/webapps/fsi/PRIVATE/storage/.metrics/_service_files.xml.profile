<?xml version="1.0" encoding="UTF-8"?>
<sourceconnector>

  <Enabled>true</Enabled>

  <Type>static</Type>

  <Origin>
    <Accessor>filesystem</Accessor>
    <!--  relative to PRIVATE -->
    <Location>internal/service_statics</Location>
  </Origin>

  <Access>
    <Group permissionset="read_only" name="authenticated"/>
  </Access>

</sourceconnector>
