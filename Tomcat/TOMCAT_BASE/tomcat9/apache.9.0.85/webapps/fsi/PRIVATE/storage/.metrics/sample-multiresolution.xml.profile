<?xml version="1.0" encoding="UTF-8"?>
<SourceConnector>

  <Enabled>true</Enabled>

  <Type>multiresolution</Type>

  <Origin>
    <Accessor>filesystem</Accessor>
    <!--  absolute path or relative to PRIVATE -->
    <Location>samples/multi-resolution</Location>
  </Origin>

  <Access>
    <Group name="public" permissionset="public_images"/>
    <Group name="authenticated" permissionset="read_write"/>
  </Access>

  <Comment>
    Sample Images Connector
    Connector can be safely removed
  </Comment>

</SourceConnector>
