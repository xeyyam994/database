<?xml version="1.0" encoding="UTF-8"?>
<SourceConnector>

  <Enabled>true</Enabled>

  <Type>static</Type>

  <Origin>
    <Accessor>filesystem</Accessor>
    <!--  relative to PRIVATE -->
    <Location>samples/statics</Location>
  </Origin>

  <Access>
    <Group name="public" permissionset="public_statics"/>
    <Group name="authenticated" permissionset="read_write"/>
  </Access>

  <Comment>
    Sample Static Files
    Connector can be safely removed
  </Comment>

</SourceConnector>
