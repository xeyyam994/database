----------------------------------------------------------------------------
FSI Server
----------------------------------------------------------------------------

Thank you for downloading FSI Server as Web Application Archive.
This version has been developed and tested on Apache Tomcat 8.5 as well as
Oracle Java SE 8.


Documentation
-------------
https://docs.neptunelabs.com/fsi-server/


Licences
-------------
License files are stored in the "PRIVATE/licences" folder within the
FSI Server installation folder.


----------------------------------------------------------------------------
Thank you for using FSI Server!
NeptuneLabs - www.neptunelabs.com
----------------------------------------------------------------------------
