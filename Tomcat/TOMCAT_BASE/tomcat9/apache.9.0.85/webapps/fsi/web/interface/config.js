/*!
 * FSI Server - Web Interface Configuration
 */

/*! Overwrite local Domain: null for auto-detection */
var baseServerDomain = null;

/*! Path to FSI Server: null for auto-detection */
var baseServerPath = null;

/*! Use HTTP or HTTPS scheme in URLs: null for schemeless URLs */
var bUseURLsWithScheme = null;

/*! Path to FSI Viewer instance to use internally in the web interface: null for auto-detection */
var interfaceViewerBase = null;

/*! Set to true to prevent file caching in publishing previews a lways, null to automatically enable caching prevention when using a proxy */
var preventCachingInPreviews = null;

var previewWindowBackgroundColor = "#333";

/*! Default web interface skin: null for default skin */
var skin = null;

/*! Default web interface language: "auto" for browser detection */
var language = "auto";

/*! Working Area Section to show on start 
	Default: "Help"
	Possible values: "Help", "Publish", "Download", "Upload", "Info" (if "urlInfoTabContent" has been defined)
*/
var initialWorkingAreaSection = null;

/*! Hide the "Search" Tab */
var hideSearchTab = false;

/*! Hide the following publishing groups 
 possible options:
 "SSI, HTML5 Viewers, Flash Viewers, Stereoscopy, Stereoscopy HTML5, Stereoscopy Flash, Static Files" 
 */
//var hidePublishingGroups = "Flash Viewers, Stereoscopy Flash";
var hidePublishingGroups = "";

/*! Content for the Info section sample: "info/index.html" */
var urlInfoTabContent = null;

/*! Content for the Help section sample: "./help/index.html" or null for built-in help */
var urlHelpTabContent = "auto";

/*! Use auto-complete feature in location bar while you type */
var useAutoCompleteLocationBar = true;

/*! Thumbnail Quality */
var thumbQuality = 95;

/*! Mouse Wheel Accelerator */
var mouseWheelAccelerator = 1.0;

/*! default Username/Password in login screen */
var defaultUsername = "";
var defaultPassword = "";

/*! show a dialog when using the default password */
var bDefaultPasswordWarning = true;

/*! enable debug mode for generic FSI Viewer */
var debugFSIViewer = false;

/*! update images in importing or reimporting state and outstanding downloads */
var bLiveUpdateLists = true;

/*! remember view properties (view mode, thumb size, order) for each connector or globally */
var bUseGlobalViewProperties = true;

/*! use smooth scrolling in file view (thumb view) */
var bUseSmoothThumbScrolling = true;

/*! Optional URL to a custom logo in the login screen */
var loginTopLogoURL = null;
