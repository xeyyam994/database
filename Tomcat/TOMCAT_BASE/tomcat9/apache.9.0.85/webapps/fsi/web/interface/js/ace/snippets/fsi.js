ace.define("ace/snippets/fsi",["require","exports","module"], function(require, exports, module) {
"use strict";

exports.snippetText = "# FSI Viewer Snippets\n\
snippet fsi-viewer\n\
	<fsi-viewer src=\"${1:sample-images/Little-Bee.jpg}\" width=\"${2:300px}\" height=\"${3:300px}\"></fsi-viewer>\n\
snippet autoRotationSpeed\n\
	autoRotationSpeed=\"${1:-1}\"\n\
";

exports.scope = "html";

});
                (function() {
                    ace.require(["ace/snippets/fsi"], function(m) {
                        if (typeof module == "object" && typeof exports == "object" && module) {
                            module.exports = m;
                        }
                    });
                })();
            