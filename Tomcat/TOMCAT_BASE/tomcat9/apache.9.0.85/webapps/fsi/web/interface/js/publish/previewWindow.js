FSIPublishPreviewWindow = function(){

  var scriptTemplate;
  var nScriptsToLoad = 0;
  var eventDOMLoaded;


  if ( typeof window.CustomEvent !== "function" ) {

    var CustomEvent = function (event, params) {
      params = params || {bubbles: false, cancelable: false, detail: undefined};
      var evt = document.createEvent('CustomEvent');
      evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
      return evt;
    };

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
  }


  var init = function(){
    scriptTemplate = document.createElement("script");
    eventDOMLoaded = new window.CustomEvent('DOMContentLoaded');
  };


  this.loadFSIPreviewScripts = function(arRequiredScriptPaths){
    if (window.$FSI) {
      // destroy existing TouchZoom and QuickZoom classes
      // so that the class initializes (DomContentLoaded event)
      delete window.$FSI.TouchZoom;
      delete window.$FSI.QuickZoom;

      // destroy $FSI.touchZoom
      if (window.$FSI.touchZoom) {
        window.$FSI.touchZoom.destroy();
        delete window.$FSI.touchZoom;
      }

      // destroy $FSI.quickZoom
      if (window.$FSI.quickZoom) {
        window.$FSI.quickZoom.destroy();
        delete window.$FSI.quickZoom;
      }

    }

    // add the scripts with "load" listeners
    nScriptsToLoad = arRequiredScriptPaths.length;

    for (var i = 0; i < arRequiredScriptPaths.length; i++) {
      var script = scriptTemplate.cloneNode(true);
      script.addEventListener("load", onPreviewScriptLoaded);

      script.setAttribute("src", arRequiredScriptPaths[i]);
      document.body.appendChild(script);
    }
  };


  var onPreviewScriptLoaded = function(el){
    nScriptsToLoad--;
    if (nScriptsToLoad === 0) onScriptsLoaded();
  };


  var onScriptsLoaded = function(){
    if (window.$FSI) {
      window.$FSI.initCustomTags();
    }

    window.dispatchEvent(eventDOMLoaded);
  };

  init();
};

function initPreview(){
  window.iFSIPreviewWindow = new FSIPublishPreviewWindow();
  window.parent.interfaceControlLocal.getPublishEditor().setPreviewDocument(window);
}
