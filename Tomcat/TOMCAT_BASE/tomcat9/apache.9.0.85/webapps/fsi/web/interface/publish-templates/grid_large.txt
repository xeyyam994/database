<fsi-imagegrid style="text-align:center;width:100%; height:100%;"
  dir=""
  cellWidth="464"
  cellHeight="330"
  preloadCount="60"
  %%Attributes%%
>
  <fsi-imagegrid-template>
    <div class="myImageGridImage">
      <img class="fsi-image-grid-image"/>
    </div>
    <div class="myImageGridTitle" >
      ###image.fileName###
    </div>
  </fsi-imagegrid-template>
</fsi-imagegrid>

<style type="text/css">
    .fsi-imagegrid-root .fsi-imagegrid-inner-container > div:hover {
      border-color:#CCC;
      -webkit-box-shadow: 0px 0px 0px 2px #CCC;
      box-shadow: 0px 0px 0px 2px #CCC;
    }

    fsi-imagegrid .fsi-imagegrid-root .myImageGridTitle{
      padding:1px 6px;
      font-size:14px;
      text-align:right;
      height:20px;
      background-color:#111;
      color:#FFF;
      overflow:hidden;
      text-overflow:ellipsis;
      white-space: nowrap;
    }
    fsi-imagegrid .fsi-imagegrid-root .myImageGridImage{
      height:calc(100% - 20px)
    }
</style>
