{
  "name": "FSI Pages",
  "tagName": "fsi-pages",
  "supports": {
    "plugins": true,
    "pageshyperlinks": true,
    "crop": true,
    "effects": true
  },
  "scriptPaths": [
    "applications/pages/js/fsipages.js"
  ],
  "defaultCompleter": "parameter",
  "parameterGroups": [
    {
      "group": " General",
      "parameters": [
        {
          "parameter": "debug",
          "example": "debug=\"true\" ",
          "type": "boolean",
          "tip": "Enable/disable debug output to the browser console.<br/>Output will be prefixed by the instance name and either the node id or a numeric id."
        },
        {
          "parameter": "width",
          "example": "width=\"640\" ",
          "type": "int",
          "tip": "Defines the width of the viewer instance in px or %."
        },
        {
          "parameter": "height",
          "example": "height=\"480\" ",
          "type": "int",
          "tip": "Defines the height of the viewer instance in px or %."
        },
        {
          "parameter": "dir",
          "example": "dir=\"/sample-images/Collection\" ",
          "type": "imageDir",
          "tip": "The path to the source images for the catalog on FSI Server."
        },
        {
          "parameter": "cfg",
          "example": "cfg=\"\" ",
          "type": "configPath",
          "tip": "The relative path to an XML configuration file in FSI Pages format."
        }
      ]
    },
    {
      "group": "Demo Mode",
      "parameters": [
        {
          "parameter": "demoURL",
          "example": "demoURL=\"enter fullscreen\" ",
          "type": "string",
          "tip": "URL to the website that should be opened on click (or alternatively, go to FullScreen)"
        },
        {
          "parameter": "demoURLTarget",
          "example": "demoURLTarget=\"_self\" ",
          "type": "string",
          "tip": "The target frame for DemoURL to open in."
        },
        {
          "parameter": "hideUIInDemoMode",
          "example": "hideUIInDemoMode=\"true\" ",
          "type": "boolean",
          "tip": "Hides UI elements in demo mode."
        },
        {
          "parameter": "demoToolTip",
          "example": "demoToolTip=\"Click for fullscreen\" ",
          "type": "string",
          "tip": "Tool tip to show when in demo mode."
        }
      ]
    },
    {
      "group": "Links",
      "parameters": [
        {
          "parameter": "links",
          "example": "links=\"true\" ",
          "type": "boolean",
          "tip": "Specifies whether links are displayed. If set to \"false\" Links and tool tips will not be displayed."
        },
        {
          "parameter": "linkRGBANormal",
          "example": "linkRGBANormal=\"0000ff88\" ",
          "type": "colorRGBA",
          "tip": "8-digit hexadecimal number specifying the color and opacity of links on the pages in normal state in the form \"RRGGBBAA\"."
        },
        {
          "parameter": "linkRGBAHover",
          "example": "linkRGBAHover=\"0000ff88\" ",
          "type": "colorRGBA",
          "tip": "8-digit hexadecimal number specifying the color and opacity of links on the pages in hover state in the form \"RRGGBBAA\"."
        },
        {
          "parameter": "linkRGBAActive",
          "example": "linkRGBAActive=\"0000ff88\" ",
          "type": "colorRGBA",
          "tip": "8-digit hexadecimal number specifying the color and opacity of links on the pages in active state in the form \"RRGGBBAA\"."
        },
        {
          "parameter": "followLinks",
          "example": "followLinks=\"true\" ",
          "type": "boolean",
          "tip": "Setting this parameter to \"false\" prevents FSI Pages from opening links on the pages. Links and tool tips will nevertheless be displayed."
        },
        {
          "parameter": "linkTemplates",
          "example": "linkTemplates=\"\" ",
          "type": "string",
          "tip": "You can provide one or more identifiers that will be replaced in Link URLs."
        },
        {
          "parameter": "linkTemplateData",
          "example": "linkTemplateData=\"\" ",
          "type": "string",
          "tip": "When defining \"LinkTemplates\" to replace place holders in Link URLs you can use this parameter to specify the values that shall be inserted."
        },
        {
          "parameter": "forceLinkURL",
          "example": "forceLinkURL=\"http://www.example.com\" ",
          "type": "string",
          "tip": "Force FSI Pages to use this URL for all links on all pages. This overwrites any URLs specified in the page data."
        },
        {
          "parameter": "forceLinkURLPrefix",
          "example": "forceLinkURLPrefix=\"http://www.example.com/&amp;searchparam=\" ",
          "type": "string",
          "tip": "Force FSI Pages to use this prefix for all links on all pages. This overwrites any prefixes specified in the page data."
        },
        {
          "parameter": "forceLinkURLSuffix",
          "example": "forceLinkURLSuffix=\"&amp;article=1234\" ",
          "type": "string",
          "tip": "Force FSI Pages to use this suffix for all links on all pages. This overwrites any suffixes specified in the page data."
        },
        {
          "parameter": "defaultLinkURL",
          "example": "defaultLinkURL=\"http://www.example.com\" ",
          "type": "string",
          "tip": "Use this URL for links if no URL has been specified in the page data."
        },
        {
          "parameter": "defaultLinkURLPrefix",
          "example": "defaultLinkURLPrefix=\"http://www.example.com/&amp;searchparam=\" ",
          "type": "string",
          "tip": "Use this prefix for all links on the pages if no prefix has been specified in the page data."
        },
        {
          "parameter": "defaultLinkURLSuffix",
          "example": "defaultLinkURLSuffix=\"&amp;article=1234\" ",
          "type": "string",
          "tip": "Use this suffix for all links on the pages if no suffix has been specified in the page data."
        },
        {
          "parameter": "defaultLinkTarget",
          "example": "defaultLinkTarget=\"_blank \" ",
          "type": "string",
          "tip": "Use this HTML target frame for links if no target frame has been specified in the page XML data."
        },
        {
          "parameter": "forceLinkTarget",
          "example": "forceLinkTarget=\"_blank\" ",
          "type": "string",
          "tip": "Force FSI Pages to use this HTML target frame for all links on all pages."
        },
        {
          "parameter": "forceJavascriptTarget",
          "example": "forceJavascriptTarget=\"\" ",
          "type": "string",
          "tip": "Force FSI Pages to use this HTML target frame for all \"javascript:\" links on all pages. This overwrites any target frames specified in the page XML data."
        },
        {
          "parameter": "defaultJavascriptTarget",
          "example": "defaultJavascriptTarget=\"\" ",
          "type": "string",
          "tip": "Use this HTML target frame for \"javascript:\" links if no target frame has been specified in the page data."
        },
        {
          "parameter": "forceLinkTip",
          "example": "forceLinkTip=\"Click to visit the web shop!\" ",
          "type": "string",
          "tip": "Force FSI Pages to use this tool tip for all links on the pages. This overwrites any tool tips specified in the page data."
        },
        {
          "parameter": "defaultLinkTip",
          "example": "defaultLinkTip=\"Click to visit the web shop!\" ",
          "type": "string",
          "tip": "Use this tool tip for all links on the pages if no tool tip has been specified in the page data."
        }
      ]
    },
    {
      "group": "Image List",
      "parameters": [
        {
          "parameter": "imageListSort",
          "example": "imageListSort=\"\" ",
          "type": "string",
          "tip": "Defines the property to sort the the image list by.<br/>Default is \"fileName\".",
          "bShowEmptyOption": false,
          "options": [
            "fileName",
            "width",
            "height",
            "resolution",
            "lastModified",
            "importStatus"
          ]
        },
        {
          "parameter": "imageListSortOrder",
          "example": "imageListSortOrder=\"\" ",
          "type": "string",
          "tip": "Defines the sort order of the images in the list.<br/>Default is \"asc\".",
          "bShowEmptyOption": false,
          "options": [
            "asc",
            "desc"
          ]
        },
        {
          "parameter": "imageListLimit",
          "example": "imageListLimit=\"0,10\" ",
          "type": "string",
          "tip": "Limits the image list to a certain amount of images within the directory."
        },
        {
          "parameter": "listFilterPositive",
          "example": "listFilterPositive=\"/8.TIF$/i\" ",
          "type": "string",
          "tip": "Filters the image list to a certain amount of images within the directory using RegEx."
        },
        {
          "parameter": "listFilterNegative",
          "example": "listFilterNegative=\"/8.TIF$/i\" ",
          "type": "string",
          "tip": "Filters the image list to a certain amount of images within the directory using RegEx."
        },
        {
          "parameter": "listTemplate",
          "example": "listTemplate=\"image_list\" ",
          "type": "string",
          "tip": "The XML based template to use when retrieving image lists from the image server via \"dir\" or \"query\" parameters. Available templates are located in the \"WEB-INF/templates/fsi/\" directory of your server setup."
        },
        {
          "parameter": "pageDataTemplate",
          "example": "pageDataTemplate=\"\" ",
          "type": "string",
          "tip": "The XML based template used for the Page Data."
        }
      ]
    },
    {
      "group": "Images",
      "parameters": [
        {
          "parameter": "effects",
          "example": "effects=\"sepia(),brightness(-30)\" ",
          "type": "string",
          "tip": "Defines server side image effects to apply to the image(s)."
        },
        {
          "parameter": "cropRect",
          "example": "cropRect=\"0,0,0.5,0.5\" ",
          "type": "string",
          "tip": "Defines the crop region (left, top, right, bottom) for all images."
        },
        {
          "parameter": "cropValues",
          "example": "cropValues=\"0,0,0.5,0.5\" ",
          "type": "string",
          "tip": "Defines the crop margins (left, top, right, bottom) for all images."
        },
        {
          "parameter": "quality",
          "example": "quality=\"50\"",
          "type": "string",
          "tip": "Defines the jpeg quality of the image [0...100]."
        },
        {
          "parameter": "imageRenderer",
          "example": "imageRenderer=\"png\" ",
          "type": "string",
          "tip": "Defines the renderer FSI Server uses to deliver images."
        },
        {
          "parameter": "imageFormat",
          "example": "imageFormat=\"png\" ",
          "type": "string",
          "tip": "Defines the file format FSI Server uses to deliver images in."
        },
        {
          "parameter": "overlays",
          "example": "overlays=\"\" ",
          "type": "string",
          "tip": "The overlays parameter to use for image requests."
        }
      ]
    },
    {
      "group": "Layout/ Appearance",
      "parameters": [
        {
          "parameter": "pageLayout",
          "example": "pageLayout=\"\" ",
          "type": "string",
          "tip": "Defines the animation when moving to another page.",
          "bShowEmptyOption": false,
          "options": [
            "flip",
            "scroll"
          ],
          "optionDescriptions": [
            "Slides the pages as if they were positioned next to each other.",
            "Shows a page turn effect."
          ]
        },
        {
          "parameter": "noEdgeFlipBelowWidth",
          "example": "noEdgeFlipBelowWidth=\"300\" ",
          "type": "int",
          "tip": "Defines if edge flip is turned off below a certain site width. Example: If set to 400, the edge flip is turned off when the page is displayed below 400px."
        },
        {
          "parameter": "dropShadow ",
          "example": "dropShadow =\"false\" ",
          "type": "boolean",
          "tip": "Adds a drop shadow underneath double pages. Only applies if PageLayout is set to \"flip\"."
        },
        {
          "parameter": "dropShadowOpacity ",
          "example": "dropShadowOpacity =\"0.8\" ",
          "type": "float",
          "tip": "Defines the drop shadow opacity."
        },
        {
          "parameter": "dropShadowDistance ",
          "example": "dropShadowDistance =\"70\" ",
          "type": "float",
          "tip": "Defines the distance of the drop shadow. Default is 50. Any value between 0.0 and 100.0 can be set."
        },
        {
          "parameter": "initialPage",
          "example": "initialPage=\"2\" ",
          "type": "int",
          "tip": "Specifies the page to display on startup, e.g. \"1\" for the front cover."
        },
        {
          "parameter": "ratio",
          "example": "ratio=\"4:3\" ",
          "type": "string",
          "tip": "The aspect ratio of pages displayed in FSI Pages. Specify \"auto\" to use the aspect ratio of the first image in the image collection."
        },
        {
          "parameter": "autoFlipCropRect",
          "example": "autoFlipCropRect=\"false\" ",
          "type": "boolean",
          "tip": "If this parameter is enabled, the right page uses a horizontally flipped crop rect. If it is set to false, both pages use the same crop rect."
        },
        {
          "parameter": "mousewheelNavigation",
          "example": "mousewheelNavigation=\"true\" ",
          "type": "boolean",
          "tip": "Defines if navigation with the mousewheel is possible. If activated, you can turn pages while scrolling with the mousewheel."
        },
        {
          "parameter": "thumbSize",
          "example": "thumbSize=\"100\" ",
          "type": "int",
          "tip": "The maximum size of thumbnails displayed in the index. The actual width and height depend on the aspect ratio of the pages."
        },
        {
          "parameter": "verticalLayout",
          "example": "verticalLayout=\"false\" ",
          "type": "boolean",
          "tip": "When set to true, the pages scroll in the vertical direction instead of the usual horizontal. Only if pageLayout is set to \"scroll\"."
        },
        {
          "parameter": "onePageInView",
          "example": "onePageInView=\"false\" ",
          "type": "boolean",
          "tip": "Sets only one page in view. Only if pageLayout is set to \"scroll\"."
        },
        {
          "parameter": "doublePageMode",
          "example": "doublePageMode=\"true\" ",
          "type": "boolean",
          "tip": "If set to \"false\", the pages will not be displayed as coherent double pages."
        },
        {
          "parameter": "autoDoublePageMode",
          "example": "autoDoublePageMode=\"true\" ",
          "type": "boolean",
          "tip": "Sets the double page mode automatically."
        },
        {
          "parameter": "bendEffect",
          "example": "bendEffect=\"\" ",
          "type": "string",
          "tip": "Defines if the pages are shown with a bend effect.",
          "bShowEmptyOption": false,
          "options": [
            "glossy",
            "none"
          ],
          "optionDescriptions": [
            "Defines if the pages are shown with a bend effect.",
            "Disables the bend effect."
          ]
        },
        {
          "parameter": "frontCover",
          "example": "frontCover=\"true\" ",
          "type": "boolean",
          "tip": "If set to false, this disables access to the front cover and forces FSI Pages to display the first image on the first inner left page."
        },
        {
          "parameter": "backCover",
          "example": "backCover=\"true\" ",
          "type": "boolean",
          "tip": "If set to false, this disables access to the back cover and forces FSI Pages to add a blank content page if required."
        },
        {
          "parameter": "frontCoverImage",
          "example": "frontCoverImage=\"\" ",
          "type": "string",
          "tip": "A path to an image to use for the front cover. FSI Pages will use the \"ImageServer\" parameter of FSI Viewer for relative paths. The cover page(s) will be added to the images in the image list"
        },
        {
          "parameter": "backCoverImage",
          "example": "backCoverImage=\"\" ",
          "type": "string",
          "tip": "A path to an image to use for the back cover. FSI Pages will use the \"ImageServer\" parameter of FSI Viewer for relative paths. The cover page(s) will be added to the images in the image list"
        },
        {
          "parameter": "blankBackCover",
          "example": "blankBackCover=\"true\" ",
          "type": "boolean",
          "tip": "Use a blank page as back cover."
        },
        {
          "parameter": "emptyImages",
          "example": "emptyImages=\"2,10\" ",
          "type": "string",
          "tip": "Using this parameter you can add blank pages. The value of the parameter must contain one or more image page numbers of the blank pages to be inserted."
        },
        {
          "parameter": "removePages",
          "example": "removePages=\"1,3,4\" ",
          "type": "string",
          "tip": "Using this parameter you can remove images from the image collection. The behavior of FSI Pages is exactly as if the images would not be listed in the image collection."
        },
        {
          "parameter": "idleAutoTurn",
          "example": "idleAutoTurn=\"false\" ",
          "type": "boolean",
          "tip": "Run the FSI Pages in demonstration mode and turn pages automatically when the user does not interact (= moves the mouse) for 2 seconds."
        },
        {
          "parameter": "idleAutoTurnMinPage",
          "example": "idleAutoTurnMinPage=\"4\" ",
          "type": "int",
          "tip": "Loop the auto flip action between IdleAutoTurnMinPage and IdleAutoTurnMaxPage. This way you can restrict the auto flip to a range of pages. If the initial page is outside the range specified, FSI Pages flips towards the range before looping between the range specified."
        },
        {
          "parameter": "idleAutoTurnMaxPage",
          "example": "idleAutoTurnMaxPage=\"19\" ",
          "type": "int",
          "tip": "Loop the auto flip action between IdleAutoTurnMinPage and IdleAutoTurnMaxPage. This way you can restrict the auto flip to a range of pages. If the initial page is outside the range specified, FSI Pages flips towards the range before looping between the range specified. "
        },
        {
          "parameter": "idleAutoTurnDelay",
          "example": "idleAutoTurnDelay=\"4\" ",
          "type": "float",
          "tip": "Show each page for at least n seconds when flipping pages automatically. Please note that a page might be displayed for longer if loading the page takes longer than the time specified."
        },
        {
          "parameter": "pageNumbers",
          "example": "pageNumbers=\"false\" ",
          "type": "boolean",
          "tip": "Show or hide page numbers on pages."
        },
        {
          "parameter": "firstPageNumber",
          "example": "firstPageNumber=\"1\" ",
          "type": "int",
          "tip": "Defines with which page number the catalog starts. By default (FirstPageNumber=1) the front cover is page number one"
        },
        {
          "parameter": "useRomanPageNumbersToPage",
          "example": "useRomanPageNumbersToPage=\"4\" ",
          "type": "int",
          "tip": "Defines the page number up to which FSI Pages uses Roman page numbers."
        },
        {
          "parameter": "romanPageNumbersOffset",
          "example": "romanPageNumbersOffset=\"0\" ",
          "type": "int",
          "tip": "Defines the value of the first Roman page number."
        },
        {
          "parameter": "customPageNumbers",
          "example": "customPageNumbers=\"\" ",
          "type": "",
          "tip": "Use \"CustomPageNumbers\" to specify custom page numbers as a comma separated string. Items starting with underscore (\"_\") will not be displayed."
        },
        {
          "parameter": "customPageNumbersFile",
          "example": "customPageNumbersFile=\"\" ",
          "type": "string",
          "tip": "Same as CustomPageNumbers, but the page number values will be loaded from an XML file. You can either define a path relative to the fsi/config/pages_xml/ directory or an absolute URL."
        },
        {
          "parameter": "rememberLastViewedPage",
          "example": "rememberLastViewedPage=\"false\" ",
          "type": "boolean",
          "tip": "If you enable this parameter FSI Pages stores the page number of the most recently viewed page on the user's computer and displays this page on start up the next time the user views the same catalogue (image collection) again."
        },
        {
          "parameter": "rememberLastViewedPageExpireAfter",
          "example": "rememberLastViewedPageExpireAfter=\"3600\" ",
          "type": "int",
          "tip": "By default the last viewed page will be restored on the next visit of a user if you enable the RememberLastViewedPage parameter. You might want to specify an expire time (in seconds) after that the last viewed page will be discarded. "
        }
      ]
    },
    {
      "group": "Search",
      "parameters": [
        {
          "parameter": "search",
          "example": "search=\"false\" ",
          "type": "boolean",
          "tip": "Enables or disables the search interface of FSI Pages."
        },
        {
          "parameter": "searchInput",
          "example": "searchInput=\"true\" ",
          "type": "boolean",
          "tip": "Shows the input for search."
        },
        {
          "parameter": "searchType",
          "example": "searchType=\"simplesearch\" ",
          "type": "string",
          "tip": "Using \"simplesearch\" makes FSI Server search in the data fields \"iptc.fsi_search_data\" and \"iptc.caption\" only by default rather than searching in all meta data fields."
        },
        {
          "parameter": "searchContext",
          "example": "searchContext=\"iptc.fsi_search_data,iptc.caption, iptc.fsi_extra\" ",
          "type": "string",
          "tip": "Comma separated list of meta data fields names to search in."
        },
        {
          "parameter": "searchAutoWildCards",
          "example": "searchAutoWildCards=\"true\" ",
          "type": "boolean",
          "tip": "By default FSI Pages adds wildcards (\"*\") to the keywords the user entered into the search input of the search dialog. Set this parameter to \"false\" if you do not want FSI Pages to add wildcards automatically."
        },
        {
          "parameter": "searchAutoSelectFirstResult",
          "example": "searchAutoSelectFirstResult=\"false\" ",
          "type": "boolean",
          "tip": "Select the first search result item when the search is complete (true, default) or just show the results (false)."
        },
        {
          "parameter": "searchResultClickAction",
          "example": "searchResultClickAction=\"\" ",
          "type": "string",
          "tip": "Defines action when search result is clicked.",
          "options": [
            "flip",
            "zoom"
          ],
          "optionDescriptions": [
            "flip to result page (default)",
            "zoom the result page"
          ]
        },
        {
          "parameter": "searchTextOnStart",
          "example": "searchTextOnStart=\"\" ",
          "type": "string",
          "tip": "Define a text to search for on start or an empty string (default) to not initiate a text search on start."
        },
        {
          "parameter": "searchThumbSize",
          "example": "searchThumbSize=\"220\" ",
          "type": "int",
          "tip": "Defines the size of the Search result thumbnails."
        },
        {
          "parameter": "searchParameters",
          "example": "searchParameters=\"\" ",
          "type": "string",
          "tip": "This parameter provides the possibility to add parameters to the search query within FSI Pages. The parameters need to be provided in the regular HTTP request format."
        },
        {
          "parameter": "searchCustomURL",
          "example": "searchCustomURL=\"\" ",
          "type": "string",
          "tip": "You can assign a custom URL to direct search requests to using this parameter. For example you might want to use a database and server side script to return pages related to the search request. "
        },
        {
          "parameter": "searchResultServerTemplate",
          "example": "searchResultServerTemplate=\"pages_searchresult\" ",
          "type": "string",
          "tip": "Specifies the server template to request in search queries."
        },
        {
          "parameter": "searchUseMethodGet",
          "example": "searchUseMethodGet=\"false\" ",
          "type": "boolean",
          "tip": "By default FSI Pages requests search results using the HTTP POST method. Using this parameter you can force FSI Pages to use the HTTP GET method when requesting search results."
        },
        {
          "parameter": "searchSortResults",
          "example": "searchSortResults=\"true\" ",
          "type": "boolean",
          "tip": "If set to true, the search results are ordered by page number. If set to false, the search results are listed in the order the imaging server sends them to FSI Pages."
        }
      ]
    },
    {
      "group": "Print/Save",
      "parameters": [
        {
          "parameter": "print",
          "example": "print=\"true\" ",
          "type": "boolean",
          "tip": "Enable or disable the user to print current pages, the complete catalog or specific pages from the image collection shown with FSI Pages."
        },
        {
          "parameter": "printResolution",
          "example": "printResolution=\"2000\" ",
          "type": "int",
          "tip": "Specifies in which resolution the user is allowed to print the pages."
        },
        {
          "parameter": "printEffects",
          "example": "printEffects=\"sharpen=100&quality=95\" ",
          "type": "string",
          "tip": "Defines image modification parameters to apply to images used for printing."
        },
        {
          "parameter": "save",
          "example": "save=\"true\" ",
          "type": "boolean",
          "tip": "Enable or disable the download dialog where the user can save the current pages or the complete catalog, depending on the settings."
        },
        {
          "parameter": "saveResolution",
          "example": "saveResolution=\"2000\" ",
          "type": "int",
          "tip": "Specifies in which resolution the user is allowed to save the pages."
        },
        {
          "parameter": "saveEffects",
          "example": "saveEffects=\"sharpen=100&quality=95\" ",
          "type": "string",
          "tip": "Defines image modification parameters to apply to images used for saving."
        },
        {
          "parameter": "saveDocumentFile",
          "example": "saveDocumentFile=\"\" ",
          "type": "string",
          "tip": "If you want to enable the user to save the PDF of your catalog, you can link the URL to the PDF here. "
        }
      ]
    },
    {
      "group": "User Interface",
      "parameters": [
        {
          "parameter": "skin",
          "example": "skin=\"\" ",
          "type": "string",
          "tip": "Defines the UI skin to use.",
          "options": [
            "black",
            "silver",
            "white"
          ],
          "optionDescriptions": [
            "Uses the black default skin.",
            "Uses the silver default skin.",
            "Uses the white default skin."
          ]
        },
        {
          "parameter": "hideUI",
          "example": "hideUI=\"true\" ",
          "type": "boolean",
          "tip": "Defines the visibility of the UI."
        },
        {
          "parameter": "language",
          "example": "language=\"english\" ",
          "type": "string",
          "tip": "The language parameter can be used to adapt the tooltips which are shown while hovering over a button to a certain language. "
        },
        {
          "parameter": "menuButtonOrder",
          "example": "menuButtonOrder=\"ToggleFullscreen, FirstPage, LastPage\" ",
          "type": "string",
          "tip": "Defines the button order of the menu bar buttons. The buttons need to be listed in a string with the corresponding button IDs."
        },
        {
          "parameter": "adaptiveUISize",
          "example": "adaptiveUISize=\"true\" ",
          "type": "boolean",
          "tip": "When set to true, the interface scales according to the zoom level."
        },
        {
          "parameter": "zoom",
          "example": "zoom=\"true\" ",
          "type": "boolean",
          "tip": "Enables/ disables page zoom."
        },
        {
          "parameter": "maxZoom",
          "example": "maxZoom=\"50\" ",
          "type": "float",
          "tip": "Maximum zoom in percent of the source image."
        },
        {
          "parameter": "oneClickZoom",
          "example": "oneClickZoom=\"false\" ",
          "type": "boolean",
          "tip": "Enables zooming into the catalog with one click instead of the default double click setting."
        },
        {
          "parameter": "buttonsFirstLastPage",
          "example": "buttonsFirstLastPage=\"true\" ",
          "type": "boolean",
          "tip": "When set to true, the buttos for directing to the first or the last page appear."
        },
        {
          "parameter": "index",
          "example": "index=\"true\" ",
          "type": "boolean",
          "tip": "Enables or disables the page index containing thumbnails of all pages."
        },
        {
          "parameter": "pageInput",
          "example": "pageInput=\"true\" ",
          "type": "boolean",
          "tip": "Specifies whether a text box for the page input is displayed in the menu bar."
        },
        {
          "parameter": "pageInputEnterButton",
          "example": "pageInputEnterButton=\"true\" ",
          "type": "boolean",
          "tip": "Specifies whether the enter button for the page input field is displayed in the menu bar."
        }
      ]
    },
    {
      "group": "Plugin FullScreen",
      "pluginName": "fullScreen",
      "parameters": [
        {
          "parameter": "disable FullScreen plugin",
          "example": "fullScreen=\"false\" ",
          "type": "boolean",
          "tip": "Enable or disable the plugin."
        }
      ]
    },
    {
      "group": "Plugin Chapters",
      "pluginName": "chapters",
      "parameters": [
        {
          "parameter": "chapters_indexDataFile",
          "example": "chapters_indexDataFile=\"\" ",
          "type": "configPath",
          "tip": "You can use an external XML file to provide the XML document structure data required for this plug-in."
        },
        {
          "parameter": "chapters_width",
          "example": "chapters_width=\"200\" ",
          "type": "int",
          "tip": "Usually the FSI Pages skin defines the width and position of the select box. Alternatively you can specify a fixed width in pixels using this parameter."
        }
      ]
    },
    {
      "group": "Plugin Bookmarks",
      "pluginName": "bookmarks",
      "parameters": [
        {
          "parameter": "bookmarks_thumbSize",
          "example": "bookmarks_thumbSize=\"150\" ",
          "type": "float",
          "tip": "Defines the maximum thumbnail size in the bookmark list in pixel."
        },
        {
          "parameter": "bookmarks_showIconsOnPages",
          "example": "bookmarks_showIconsOnPages=\"true\" ",
          "type": "boolean",
          "tip": "Shows icon on bookmarked pages."
        },
        {
          "parameter": "bookmarks_pageNumbers",
          "example": "bookmarks_pageNumbers=\"true\" ",
          "type": "boolean",
          "tip": "Shows page numbers in bookmark list."
        },
        {
          "parameter": "bookmarks_persistentStorage",
          "example": "bookmarks_persistentStorage=\"true\" ",
          "type": "boolean",
          "tip": "When disabled, the bookmarks will be deleted when the user leaves the FSI Pages instance."
        },
        {
          "parameter": "bookmarks_persistentStorageExpiresAfter",
          "example": "bookmarks_persistentStorageExpiresAfter=\"3600\" ",
          "type": "int",
          "tip": "Defines after which period of time the cached note data expires."
        }
      ]
    },
    {
      "group": "Plugin PagesThumbBar",
      "pluginName": "pagesThumbBar",
      "parameters": [
        {
          "parameter": "pagesThumbBar_pageNumbers",
          "example": "pagesThumbBar_pageNumbers=\"true\" ",
          "type": "boolean",
          "tip": "Enables/ disables the display of page numbers being below the thumbnails."
        },
        {
          "parameter": "pagesThumbBar_autohide",
          "example": "pagesThumbBar_autohide=\"true\" ",
          "type": "boolean",
          "tip": "If set to true, the thumbnails will not be displayed if the FSI Pages Instance is smaller than 600px."
        },
        {
          "parameter": "pagesThumbBar_height",
          "example": "pagesThumbBar_height=\"140\" ",
          "type": "int",
          "tip": "Defines the height of the thumbnails displayed."
        }
      ]
    }
  ]
}
