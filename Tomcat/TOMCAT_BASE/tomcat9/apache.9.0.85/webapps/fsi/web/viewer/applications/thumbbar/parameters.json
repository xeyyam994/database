{
  "name": "FSI ThumbBar",
  "tagName": "fsi-thumbbar",
  "supports": {
    "crop": true,
    "effects": true,
    "areaEffects": true
  },
  "scriptPaths": [
    "applications/touchzoom/js/fsitouchzoom.js",
    "applications/thumbbar/js/fsithumbbar.js"

  ],
  "defaultCompleter": "parameter",
  "parameterGroups": [
    {
      "group": " General",
      "parameters": [
        {
          "parameter": "debug",
          "example": "debug=\"true\" ",
          "type": "boolean",
          "tip": "Enable/disable debug output to the browser console.<br/>Output will be prefixed by the instance name and either the node id or a numeric id."
        },
        {
          "parameter": "dir",
          "example": "dir=\"\"",
          "type": "imageDir",
          "tip": "Defines the path to the source image directory."
        },
        {
          "parameter": "cfg",
          "example": "cfg=\"\" ",
          "type": "configPath",
          "tip": "The relative path to an XML configuration file in FSI Viewer format."
        },
        {
          "parameter": "language",
          "example": "language=\"english\" ",
          "type": "string",
          "tip": "The language parameter can be used to adapt the tooltips which are shown while hovering over a button to a certain language. "
        },
        {
          "parameter": "width",
          "example": "width=\"640\" ",
          "type": "int",
          "tip": "Defines the width of the viewer instance in px or %."
        },
        {
          "parameter": "height",
          "example": "height=\"480\" ",
          "type": "int",
          "tip": "Defines the height of the viewer instance in px or %."
        },
        {
          "parameter": "autoDestroy",
          "example": "autoDestroy=\"true\" ",
          "type": "boolean",
          "tip": "Automatically destroy instances created with the fsi-thumbbar tag upon removing the tag from the document DOM."
        },
        {
          "parameter": "viewerSelector",
          "example": "viewerSelector=\"fsi-viewer\" ",
          "type": "string",
          "tip": "Defines a selector,e.g. #myViewer or fsi-viewer or .myClass of an FSI Viewer instance to be used to display the selected image."
        },
        {
          "parameter": "cmdButtonSelector",
          "example": "cmdButtonSelector=\"#myThumbBarControls > input\" ",
          "type": "string",
          "tip": "Defines a selector to one or more DOM elements that should be used as control buttons."
        },
        {
          "parameter": "garbageCollectionSize",
          "example": "garbageCollectionSize=\"-1\" ",
          "type": "int",
          "tip": "Specifies how many images will be kept in memory although they ar not visible anymore."
        }
      ]
    },
    {
      "group": "Images",
      "parameters": [
        {
          "parameter": "effects",
          "example": "effects=\"sepia(),brightness(-30)\" ",
          "type": "string",
          "tip": "Defines server side image effects to apply to the image(s)."
        },
        {
          "parameter": "cropRect",
          "example": "cropRect=\"0,0,0.5,0.5\" ",
          "type": "string",
          "tip": "Defines the crop region (left, top, right, bottom) for all images."
        },
        {
          "parameter": "cropValues",
          "example": "cropValues=\"0,0,0.5,0.5\" ",
          "type": "string",
          "tip": "Defines the crop margins (left, top, right, bottom) for all images."
        },
        {
          "parameter": "quality",
          "example": "quality=\"50\"",
          "type": "string",
          "tip": "Defines the jpeg quality of the image [0...100]."
        },
        {
          "parameter": "imageRenderer",
          "example": "imageRenderer=\"png\" ",
          "type": "string",
          "tip": "Defines the renderer FSI Server uses to deliver images."
        },
        {
          "parameter": "imageFormat",
          "example": "imageFormat=\"png\" ",
          "type": "string",
          "tip": "Defines the file format FSI Server uses to deliver images in."
        },
        {
          "parameter": "overlays",
          "example": "overlays=\"\" ",
          "type": "string",
          "tip": "The overlays parameter to use for image requests."
        }
      ]
    },
    {
      "group": "Image List",
      "parameters": [
        {
          "parameter": "imageListSort",
          "example": "imageListSort=\"\" ",
          "type": "string",
          "tip": "Defines the property to sort the the image list by.<br/>Default is \"fileName\".",
          "bShowEmptyOption": false,
          "options": [
            "fileName",
            "width",
            "height",
            "resolution",
            "lastModified",
            "importStatus"
          ]
        },
        {
          "parameter": "imageListSortOrder",
          "example": "imageListSortOrder=\"\" ",
          "type": "string",
          "tip": "Defines the sort order of the images in the list.<br/>Default is \"asc\".",
          "bShowEmptyOption": false,
          "options": [
            "asc",
            "desc"
          ]
        },
        {
          "parameter": "imageListLimit",
          "example": "imageListLimit=\"0,10\" ",
          "type": "string",
          "tip": "Limits the image list to a certain amount of images within the directory."
        },
        {
          "parameter": "listFilterPositive",
          "example": "listFilterPositive=\"/\\/09\\d*.tif/i\" ",
          "type": "string",
          "tip": "Filters the image list to a certain amount of images within the directory using RegEx."
        },
        {
          "parameter": "listFilterNegative",
          "example": "listFilterNegative=\"/\\/09\\d*.tif/i\" ",
          "type": "string",
          "tip": "Filters the image list to a certain amount of images within the directory using RegEx."
        }
      ]
    },
    {
      "group": "Presentation",
      "parameters": [
        {
          "parameter": "presentationType",
          "example": "presentationType=\"\" ",
          "type": "string",
          "tip": "defines the type of the thumbnail presentation.",
          "bShowEmptyOption": false,
          "options": [
            "flat",
            "innerRing",
            "outerRing",
            "stacks"
          ],
          "optionDescriptions": [
            "Defines a simple scroll presentation.",
            "Defines a 3D presentation from the inside of a ring.",
            "Defines a 3D presentation from the outside of a ring.",
            "Defines a presentation with stacked images."
          ]
        },
        {
          "parameter": "thumbLabel",
          "example": "thumbLabel=\"###image.fileName###\" ",
          "type": "string",
          "tip": "String or HTML code with optional templates to display below all thumbnails.<br/>Example: ###image.fileName###&lt;hr/&gt;###iptc.Date Created###"
        },
        {
          "parameter": "autoCrop",
          "example": "autoCrop=\"cc\" ",
          "type": "string",
          "tip": "Crop images to fill entire image area. Default is false.<br>Use any combination of t,c,b and l,c,r where tl is top-left."
        },
        {
          "parameter": "autoRotateSpeed",
          "example": "autoRotateSpeed=\"3.0\" ",
          "type": "float",
          "tip": "defines the automatic scroll/rotate speed on start.<br/>Use negative values to scroll backwards."
        },
        {
          "parameter": "elementWidth",
          "example": "elementWidth=\"20\" ",
          "type": "string",
          "tip": "The maximum width of images in the slideshow. The value can be provided in pixels or in percent of the instance width."
        },
        {
          "parameter": "minElementWidth",
          "example": "minElementWidth=\"24\" ",
          "type": "int",
          "tip": "The minimum width of images in the slideshow. The value can be provided in pixels or in percent of the instance width."
        },
        {
          "parameter": "elementSpacing",
          "example": "elementSpacing=\"0.3\" ",
          "type": "string",
          "tip": "Spacing between the images in pixel. The spacing needs to be either stated in pixels or in percent of the instance width."
        },
        {
          "parameter": "minElementSpacing",
          "example": "minElementSpacing=\"1\" ",
          "type": "int",
          "tip": "Minimum spacing between the images in pixel. The spacing needs to be either stated in pixels or in percent of the instance width."
        },
        {
          "parameter": "autoElementSpacing",
          "example": "autoElementSpacing=\"false\" ",
          "type": "boolean",
          "tip": "Adjusts the element spacing automatically. Default is \"true\"."
        },
        {
          "parameter": "paddingTop",
          "example": "paddingTop=\"5\" ",
          "type": "int",
          "tip": "Top padding in pixel."
        },
        {
          "parameter": "paddingBottom",
          "example": "paddingBottom=\"5\" ",
          "type": "int",
          "tip": "Bottom padding in pixel."
        },
        {
          "parameter": "autoResize",
          "example": "autoResize=\"true\" ",
          "type": "boolean",
          "tip": "Defines if instance is resized when the DOM element is resized"
        },
        {
          "parameter": "initialImage",
          "example": "initialImage=\"2\" ",
          "type": "int",
          "tip": "Defines the image index to focus on start."
        },
        {
          "parameter": "vertical",
          "example": "vertical=\"false\" ",
          "type": "boolean",
          "tip": "Defines if the thumb bar is displayed horizontally (default) or vertically. "
        },
        {
          "parameter": "enableZoom",
          "example": "enableZoom=\"true\" ",
          "type": "boolean",
          "tip": "Enable or disable zooming the focused image with the built-in image zoom."
        },
        {
          "parameter": "useTouchZoom",
          "example": "useTouchZoom=\"true\" ",
          "type": "boolean",
          "tip": "Uses FSI TouchZoom on thumbnail images."
        },
        {
          "parameter": "zoomMargin",
          "example": "zoomMargin=\"20\" ",
          "type": "int",
          "tip": "Sets margin around the zoom div."
        },
        {
          "parameter": "scrollBar",
          "example": "scrollBar=\"false\" ",
          "type": "boolean",
          "tip": "This value enables or disables the scroll bar."
        },
        {
          "parameter": "centerZoomedImage",
          "example": "centerZoomedImage=\"true\" ",
          "type": "boolean",
          "tip": "Moves the currently zoomed image to the center of the bar."
        },
        {
          "parameter": "alignment",
          "example": "alignment=\"0.5\" ",
          "type": "float",
          "tip": "Usually, the alignment is done via CSS.<br />Alternatively, you can still use the alignment parameter: 1.0: align at bottom (or right),0.5: center images,0.0: align at top (left)"
        },
        {
          "parameter": "containerAlignment",
          "example": "containerAlignment=\"\" ",
          "type": "string",
          "tip": "Defines the alignment of the image container if there are less images than fitting into the element.<br/>Only applicable to presentationType=\"flat\" and if autoElementSpacing=\"false\". Default is \"left\".",
          "bShowEmptyOption": false,
          "options": [
            "left",
            "center",
            "right"
          ]
        },
        {
          "parameter": "imageListSortOrder",
          "example": "imageListSortOrder=\"\" ",
          "type": "string",
          "tip": "Defines the sort order of the images in the list.<br/>Default is \"asc\".",
          "bShowEmptyOption": false,
          "options": [
            "asc",
            "desc"
          ]
        },
        {
          "parameter": "endlessScrolling",
          "example": "endlessScrolling=\"true\" ",
          "type": "boolean",
          "tip": "Defines if the endless scroll is activated and duplicates images if required."
        },
        {
          "parameter": "preloadCount",
          "example": "preloadCount=\"20\" ",
          "type": "int",
          "tip": "The number of images to pre-load although they are currently not visible.<br />Disabling preload (set value to 0) will only load images visible on stage."
        },
        {
          "parameter": "depthFadeout",
          "example": "depthFadeout=\"true\" ",
          "type": "boolean",
          "tip": "Darkens images that are further away in z-direction (for 3D presentation types)."
        },
        {
          "parameter": "zoomShaderStartOpacity",
          "example": "zoomShaderStartOpacity=\"30\" ",
          "type": "float",
          "tip": "The start opacity (0 to 100) of the shader when zooming an image."
        },
        {
          "parameter": "zoomShaderEndOpacity ",
          "example": "zoomShaderEndOpacity=\"30\" ",
          "type": "float",
          "tip": "The end opacity (0 to 100) of the shader when zooming an image."
        },
        {
          "parameter": "perspectiveAlignment",
          "example": "perspectiveAlignment=\"1.0\" ",
          "type": "float",
          "tip": "Defines the perspective origin for 3D presentation types."
        },
        {
          "parameter": "placeHolderImage",
          "example": "placeHolderImage=\"default\" ",
          "type": "imagePath",
          "tip": "Defines the CSS Class and type of the placeholder while images are loading.<br/>Valid values are a path to an image or the following strings:<br/> none | default | custom"
        },
        {
          "parameter": "placeHolderPadEffect  ",
          "example": "placeHolderPadEffect=\"Pad(CC,FFFFFF)\" ",
          "type": "string",
          "tip": "Defines the pad effect to use when using a custom placeholder image."
        }
      ]
    }
  ]
}
